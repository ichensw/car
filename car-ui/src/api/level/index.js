import request from '@/utils/request'

// 查询各会员等级下的人数
export function getMemberNumber() {
  return request({
    url: '/system/grade/getMemberNumber',
    method: 'get'
  })
}
