import request from '@/utils/request'

// 查询库存管理列表
export function listManage(query) {
  return request({
    url: '/stock/manage/list',
    method: 'get',
    params: query
  })
}

// 查询库存管理详细
export function getManage(manageId) {
  return request({
    url: '/stock/manage/' + manageId,
    method: 'get'
  })
}

// 新增库存管理
export function addManage(data) {
  return request({
    url: '/stock/manage',
    method: 'post',
    data: data
  })
}

// 修改库存管理
export function updateManage(data) {
  return request({
    url: '/stock/manage',
    method: 'put',
    data: data
  })
}

// 删除库存管理
export function delManage(manageId) {
  return request({
    url: '/stock/manage/' + manageId,
    method: 'delete'
  })
}
