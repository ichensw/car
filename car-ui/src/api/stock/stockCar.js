import request from '@/utils/request'

// 查询车辆销售列表
export function listStockCar(query) {
  return request({
    url: '/stock/car/list',
    method: 'get',
    params: query
  })
}

// 查询车辆销售详细
export function getStockCar(carId) {
  return request({
    url: '/stock/car/' + carId,
    method: 'get'
  })
}

// 新增车辆销售
export function addStockCar(data) {
  return request({
    url: '/stock/car',
    method: 'post',
    data: data
  })
}

// 修改车辆销售
export function updateStockCar(data) {
  return request({
    url: '/stock/car',
    method: 'put',
    data: data
  })
}

// 删除车辆销售
export function delStockCar(carIds) {
  return request({
    url: '/stock/car/' + carIds,
    method: 'delete'
  })
}

