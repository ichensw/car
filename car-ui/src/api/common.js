import request from '@/utils/request'

// 删除文件(单个)
export function deleteFile(data) {
  return request({
    url: '/common/delete',
    method: 'post',
    data: data
  })
}

// 批量删除文件
export function deleteMultiFile(data) {
  return request({
    url: '/common/delete',
    method: 'post',
    data: data
  })
}
