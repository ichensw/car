import request from '@/utils/request'

// 查询汽车品牌列表
export function listBrand(query) {
  return request({
    url: '/system/brand/list',
    method: 'get',
    params: query
  })
}

// 查询汽车品牌详细
export function getBrand(brandId) {
  return request({
    url: '/system/brand/' + brandId,
    method: 'get'
  })
}

// 根据brandId和seriesId获取汽车品牌和系列详细信息
export function getCarBrandInfoByBrandIdAndSeriesId(brandId, seriesId) {
  return request({
    url: '/system/brand/' + brandId + '/' + seriesId,
    method: 'get'
  })
}

// 新增汽车品牌
export function addBrand(data) {
  return request({
    url: '/system/brand',
    method: 'post',
    data: data
  })
}

// 修改汽车品牌
export function updateBrand(data) {
  return request({
    url: '/system/brand',
    method: 'put',
    data: data
  })
}

// 删除汽车品牌
export function delBrand(brandId) {
  return request({
    url: '/system/brand/' + brandId,
    method: 'delete'
  })
}
