import request from '@/utils/request'

// 查询优惠活动列表
export function listActivies(query) {
  return request({
    url: '/system/activies/list',
    method: 'get',
    params: query
  })
}

// 查询优惠活动详细
export function getActivies(activiesId) {
  return request({
    url: '/system/activies/' + activiesId,
    method: 'get'
  })
}

// 查询优惠活动详细
export function getActiviesByRechargeAmount(activiesRechargeAmount) {
  return request({
    url: '/system/activies/activiesRechargeAmount/' + activiesRechargeAmount,
    method: 'get'
  })
}

// 新增优惠活动
export function addActivies(data) {
  return request({
    url: '/system/activies',
    method: 'post',
    data: data
  })
}

// 修改优惠活动
export function updateActivies(data) {
  return request({
    url: '/system/activies',
    method: 'put',
    data: data
  })
}

// 删除优惠活动
export function delActivies(activiesId) {
  return request({
    url: '/system/activies/' + activiesId,
    method: 'delete'
  })
}
