import request from '@/utils/request'

// 查询客户凭证列表
export function listCert(query) {
  return request({
    url: '/system/cert/list',
    method: 'get',
    params: query
  })
}

// 查询客户凭证详细
export function getCert(certId) {
  return request({
    url: '/system/cert/' + certId,
    method: 'get'
  })
}

// 新增客户凭证
export function addCert(data) {
  return request({
    url: '/system/cert',
    method: 'post',
    data: data
  })
}

// 修改客户凭证
export function updateCert(data) {
  return request({
    url: '/system/cert',
    method: 'put',
    data: data
  })
}

// 删除客户凭证
export function delCert(certId) {
  return request({
    url: '/system/cert/' + certId,
    method: 'delete'
  })
}
