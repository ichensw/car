import request from '@/utils/request'

// 查询汽车系列列表
export function listSeries(query) {
  return request({
    url: '/system/series/list',
    method: 'get',
    params: query
  })
}

// 查询汽车系列详细
export function getSeries(seriesId) {
  return request({
    url: '/system/series/' + seriesId,
    method: 'get'
  })
}

// 新增汽车系列
export function addSeries(data) {
  return request({
    url: '/system/series',
    method: 'post',
    data: data
  })
}

// 修改汽车系列
export function updateSeries(data) {
  return request({
    url: '/system/series',
    method: 'put',
    data: data
  })
}

// 删除汽车系列
export function delSeries(seriesId) {
  return request({
    url: '/system/series/' + seriesId,
    method: 'delete'
  })
}


// 查询汽车品牌列表
export function listBrand(query) {
  return request({
    url: '/system/brand/list',
    method: 'get',
    params: query
  })
}
