import request from '@/utils/request'

// 查询会员等级列表
export function listGrade(query) {
  return request({
    url: '/system/grade/list',
    method: 'get',
    params: query
  })
}

// 查询会员等级详细
export function getGrade(gradeId) {
  return request({
    url: '/system/grade/' + gradeId,
    method: 'get'
  })
}

// 新增会员等级
export function addGrade(data) {
  return request({
    url: '/system/grade',
    method: 'post',
    data: data
  })
}

// 修改会员等级
export function updateGrade(data) {
  return request({
    url: '/system/grade',
    method: 'put',
    data: data
  })
}

// 删除会员等级
export function delGrade(gradeId) {
  return request({
    url: '/system/grade/' + gradeId,
    method: 'delete'
  })
}
