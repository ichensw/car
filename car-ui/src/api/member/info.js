import request from '@/utils/request'

// 查询会员信息列表
export function listInfo(query) {
  return request({
    url: '/member/info/list',
    method: 'get',
    params: query
  })
}

// 查询会员信息详细
export function getInfo(memberInfoId) {
  return request({
    url: '/member/info/' + memberInfoId,
    method: 'get'
  })
}
// 根据会员卡号判断会员是否存在
export function getMemberExistFlagByMemberInfoNumber(memberInfoNumber) {
  return request({
    url: '/member/info/memberExistFlag/' + memberInfoNumber,
    method: 'get'
  })
}

// 根据会员卡号查询会员信息详细
export function getInfoByMemberInfoNumber(memberInfoNumber) {
  return request({
    url: '/member/info/memberInfoNumber/' + memberInfoNumber,
    method: 'get'
  })
}

// 新增会员信息
export function addInfo(data) {
  return request({
    url: '/member/info',
    method: 'post',
    data: data
  })
}

// 修改会员信息
export function updateInfo(data) {
  return request({
    url: '/member/info',
    method: 'put',
    data: data
  })
}

// 删除会员信息
export function delInfo(memberInfoId) {
  return request({
    url: '/member/info/' + memberInfoId,
    method: 'delete'
  })
}

// 获取用户总数
export function getUserCount() {
  return request({
    url: '/member/info/getUserCount',
    method: 'get'
  })
}

// 查询每个月的会员注册人数
export function getMonthUserCount() {
  return request({
    url: '/member/info/getMonthUserCount',
    method: 'get'
  })
}
