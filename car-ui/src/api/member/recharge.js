import request from '@/utils/request'

// 查询会员充值列表
export function listRecharge(query) {
  return request({
    url: '/member/recharge/list',
    method: 'get',
    params: query
  })
}

// 查询会员充值详细
export function getRecharge(rechargeId) {
  return request({
    url: '/member/recharge/' + rechargeId,
    method: 'get'
  })
}

// 校验会员支付密码是否正确
export function checkMemberInfoNumberAndPassword(data) {
  return request({
    url: '/member/recharge/checkPassword',
    method: 'post',
    data: data
  })
}

// 新增会员充值
export function addRecharge(data) {
  return request({
    url: '/member/recharge',
    method: 'post',
    data: data
  })
}

// 修改会员充值
export function updateRecharge(data) {
  return request({
    url: '/member/recharge',
    method: 'put',
    data: data
  })
}

// 删除会员充值
export function delRecharge(rechargeId) {
  return request({
    url: '/member/recharge/' + rechargeId,
    method: 'delete'
  })
}
