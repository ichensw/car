import request from '@/utils/request'


// 查询订单总数
export function getOrderCount() {
  return request({
    url: '/order/member/getOrderCount',
    method: 'get'
  })
}


// 查询每个月的订单总数
export function getMonthOrderCount() {
  return request({
    url: '/order/member/getMonthOrderCount',
    method: 'get'
  })
}
