import request from '@/utils/request'

// 查询车辆销售列表
export function listOrderCar(query) {
  return request({
    url: '/order/car/list',
    method: 'get',
    params: query
  })
}

// 查询车辆销售详细
export function getOrderCar(carId) {
  return request({
    url: '/order/car/' + carId,
    method: 'get'
  })
}

// 新增车辆销售
export function addOrderCar(data) {
  return request({
    url: '/order/car',
    method: 'post',
    data: data
  })
}

// 修改车辆销售
export function updateOrderCar(data) {
  return request({
    url: '/order/car',
    method: 'put',
    data: data
  })
}

// 删除车辆销售
export function delOrderCar(carIds) {
  return request({
    url: '/order/car/' + carIds,
    method: 'delete'
  })
}
