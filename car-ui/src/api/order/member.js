import request from '@/utils/request'

// 查询会员消费列表
export function listMember(query) {
  return request({
    url: '/order/member/list',
    method: 'get',
    params: query
  })
}

// 查询未结算的会员消费订单列表
export function listOrderMemberList(query) {
  return request({
    url: '/order/member/listAndNoSettlement',
    method: 'get',
    params: query
  })
}

export function checkOutOrderMemberList(data) {
  return request({
    url: '/order/member/checkOutOrderMemberList',
    method: 'put',
    data: data
  })
}



// 查询会员消费详细
export function getMember(orderMemberId) {
  return request({
    url: '/order/member/' + orderMemberId,
    method: 'get'
  })
}


// 新增会员消费
export function addMember(data) {
  return request({
    url: '/order/member',
    method: 'post',
    data: data
  })
}

// 修改会员消费
export function updateMember(data) {
  return request({
    url: '/order/member',
    method: 'put',
    data: data
  })
}

// 删除会员消费
export function delMember(data) {
  return request({
    url: '/order/member/deleteMember',
    method: 'post',
    data: data
  })
}
