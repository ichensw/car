import request from '@/utils/request'

// 查询散客消费列表
export function listSpread(query) {
  return request({
    url: '/order/spread/list',
    method: 'get',
    params: query
  })
}

// 查询散客消费详细
export function getSpread(spreadId) {
  return request({
    url: '/order/spread/' + spreadId,
    method: 'get'
  })
}

// 新增散客消费
export function addSpread(data) {
  return request({
    url: '/order/spread',
    method: 'post',
    data: data
  })
}

// 修改散客消费
export function updateSpread(data) {
  return request({
    url: '/order/spread',
    method: 'put',
    data: data
  })
}

// 删除散客消费
export function delSpread(spreadId) {
  return request({
    url: '/order/spread/' + spreadId,
    method: 'delete'
  })
}
