package cn.ichensw.system.service.impl;

import cn.ichensw.system.domain.SysCustomerCert;
import cn.ichensw.system.mapper.SysCustomerCertMapper;
import cn.ichensw.system.service.ISysCustomerCertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 客户凭证Service业务层处理
 *
 * @author jucce
 * @date 2022-11-26
 */
@Service
public class SysCustomerCertServiceImpl implements ISysCustomerCertService {
    @Autowired
    private SysCustomerCertMapper sysCustomerCertMapper;

    /**
     * 查询客户凭证
     *
     * @param certId 客户凭证主键
     * @return 客户凭证
     */
    @Override
    public SysCustomerCert selectSysCustomerCertByCertId(Long certId) {
        return sysCustomerCertMapper.selectSysCustomerCertByCertId(certId);
    }

    /**
     * 查询客户凭证列表
     *
     * @param sysCustomerCert 客户凭证
     * @return 客户凭证
     */
    @Override
    public List<SysCustomerCert> selectSysCustomerCertList(SysCustomerCert sysCustomerCert) {
        return sysCustomerCertMapper.selectSysCustomerCertList(sysCustomerCert);
    }

    /**
     * 新增客户凭证
     *
     * @param sysCustomerCert 客户凭证
     * @return 结果
     */
    @Override
    public int insertSysCustomerCert(SysCustomerCert sysCustomerCert) {
        return sysCustomerCertMapper.insertSysCustomerCert(sysCustomerCert);
    }

    /**
     * 修改客户凭证
     *
     * @param sysCustomerCert 客户凭证
     * @return 结果
     */
    @Override
    public int updateSysCustomerCert(SysCustomerCert sysCustomerCert) {
        return sysCustomerCertMapper.updateSysCustomerCert(sysCustomerCert);
    }

    /**
     * 批量删除客户凭证
     *
     * @param certIds 需要删除的客户凭证主键
     * @return 结果
     */
    @Override
    public int deleteSysCustomerCertByCertIds(Long[] certIds) {
        return sysCustomerCertMapper.deleteSysCustomerCertByCertIds(certIds);
    }

    /**
     * 删除客户凭证信息
     *
     * @param certId 客户凭证主键
     * @return 结果
     */
    @Override
    public int deleteSysCustomerCertByCertId(Long certId) {
        return sysCustomerCertMapper.deleteSysCustomerCertByCertId(certId);
    }
}
