package cn.ichensw.system.service.impl;

import cn.ichensw.common.utils.StringUtils;
import cn.ichensw.system.domain.StockProduct;
import cn.ichensw.system.domain.vo.StockProductVo;
import cn.ichensw.system.mapper.StockProductMapper;
import cn.ichensw.system.service.IStockProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 产品信息Service业务层处理
 *
 * @author jucce
 * @date 2022-11-25
 */
@Service
public class StockProductServiceImpl implements IStockProductService {
    @Autowired
    private StockProductMapper stockProductMapper;

    /**
     * 查询产品信息
     *
     * @param productId 产品信息主键
     * @return 产品信息
     */
    @Override
    public StockProduct selectStockProductByProductId(Long productId) {
        return stockProductMapper.selectStockProductByProductId(productId);
    }

    /**
     * 查询产品信息列表
     *
     * @param stockProduct 产品信息
     * @return 产品信息
     */
    @Override
    public List<StockProductVo> selectStockProductList(StockProduct stockProduct) {
        return stockProductMapper.selectStockProductList(stockProduct);
    }

    /**
     * 新增产品信息
     *
     * @param stockProduct 产品信息
     * @return 结果
     */
    @Override
    public int insertStockProduct(StockProduct stockProduct) {
        String productImg = stockProduct.getProductImg();
        if (StringUtils.isEmpty(productImg)) {
            return stockProductMapper.insertStockProduct(stockProduct);
        }

        if (productImg.endsWith(",")) {
            stockProduct.setProductImg(productImg.substring(0, productImg.length() - 1));
        }
        return stockProductMapper.insertStockProduct(stockProduct);
    }

    /**
     * 修改产品信息
     *
     * @param stockProduct 产品信息
     * @return 结果
     */
    @Override
    public int updateStockProduct(StockProduct stockProduct) {
        String productImg = stockProduct.getProductImg();
        if (productImg.endsWith(",")) {
            stockProduct.setProductImg(productImg.substring(0, productImg.length() - 1));
        }
        return stockProductMapper.updateStockProduct(stockProduct);
    }

    /**
     * 批量删除产品信息
     *
     * @param productIds 需要删除的产品信息主键
     * @return 结果
     */
    @Override
    public int deleteStockProductByProductIds(Long[] productIds) {
        return stockProductMapper.deleteStockProductByProductIds(productIds);
    }

    /**
     * 删除产品信息信息
     *
     * @param productId 产品信息主键
     * @return 结果
     */
    @Override
    public int deleteStockProductByProductId(Long productId) {
        return stockProductMapper.deleteStockProductByProductId(productId);
    }

    /**
     * 根据产品类别查询产品信息
     *
     * @param typeId 产品类别ID
     * @return 产品信息
     */
    @Override
    public List<StockProduct> selectStockProductByTypeId(Long typeId) {
        return stockProductMapper.selectStockProductByTypeId(typeId);
    }
}
