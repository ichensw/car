package cn.ichensw.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import cn.ichensw.system.domain.dto.StockCarDTO;
import cn.ichensw.system.domain.vo.StockCarVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ichensw.system.mapper.StockCarMapper;
import cn.ichensw.system.domain.StockCar;
import cn.ichensw.system.service.IStockCarService;

import javax.annotation.Resource;

/**
 * 车辆信息Service业务层处理
 * 
 * @author jucce
 * @date 2023-03-05
 */
@Service
public class StockCarServiceImpl implements IStockCarService 
{
    @Resource
    private StockCarMapper stockCarMapper;

    /**
     * 查询车辆信息
     * 
     * @param carId 车辆信息主键
     * @return 车辆信息
     */
    @Override
    public StockCar selectStockCarByCarId(Long carId)
    {
        return stockCarMapper.selectStockCarByCarId(carId);
    }

    /**
     * 查询车辆信息列表
     * 
     * @param stockCar 车辆信息
     * @return 车辆信息
     */
    @Override
    public List<StockCarVo> selectStockCarList(StockCar stockCar)
    {
        List<StockCarVo> stockCarVos = new ArrayList<>();
        List<StockCarDTO> stockCarDTOList = stockCarMapper.selectStockCarDTOList(stockCar);
        stockCarDTOList.stream().forEach(item -> {
            StockCarVo stockCarVo = new StockCarVo();
            BeanUtils.copyProperties(item, stockCarVo);
            stockCarVos.add(stockCarVo);
        });
        return stockCarVos;
    }

    /**
     * 新增车辆信息
     * 
     * @param stockCar 车辆信息
     * @return 结果
     */
    @Override
    public int insertStockCar(StockCar stockCar)
    {
        return stockCarMapper.insertStockCar(stockCar);
    }

    /**
     * 修改车辆信息
     * 
     * @param stockCar 车辆信息
     * @return 结果
     */
    @Override
    public int updateStockCar(StockCar stockCar)
    {
        return stockCarMapper.updateStockCar(stockCar);
    }

    /**
     * 批量删除车辆信息
     * 
     * @param carIds 需要删除的车辆信息主键
     * @return 结果
     */
    @Override
    public int deleteStockCarByCarIds(Long[] carIds)
    {
        return stockCarMapper.deleteStockCarByCarIds(carIds);
    }

    /**
     * 删除车辆信息信息
     * 
     * @param carId 车辆信息主键
     * @return 结果
     */
    @Override
    public int deleteStockCarByCarId(Long carId)
    {
        return stockCarMapper.deleteStockCarByCarId(carId);
    }
}
