package cn.ichensw.system.service;

import cn.ichensw.system.domain.StockManage;
import cn.ichensw.system.domain.vo.StockManageVo;

import java.util.List;

/**
 * 库存管理Service接口
 *
 * @author jucce
 * @date 2022-11-25
 */
public interface IStockManageService {
    /**
     * 查询库存管理
     *
     * @param manageId 库存管理主键
     * @return 库存管理
     */
    public StockManage selectStockManageByManageId(Long manageId);

    /**
     * 查询库存管理列表
     *
     * @param stockManage 库存管理
     * @return 库存管理集合
     */
    public List<StockManageVo> selectStockManageList(StockManage stockManage);

    /**
     * 新增库存管理
     *
     * @param stockManage 库存管理
     * @return 结果
     */
    public int insertStockManage(StockManage stockManage);

    /**
     * 修改库存管理
     *
     * @param stockManage 库存管理
     * @return 结果
     */
    public int updateStockManage(StockManage stockManage);

    /**
     * 批量删除库存管理
     *
     * @param manageIds 需要删除的库存管理主键集合
     * @return 结果
     */
    public int deleteStockManageByManageIds(Long[] manageIds);

    /**
     * 删除库存管理信息
     *
     * @param manageId 库存管理主键
     * @return 结果
     */
    public int deleteStockManageByManageId(Long manageId);
}
