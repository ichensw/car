package cn.ichensw.system.service;

import cn.ichensw.system.domain.OrderMember;
import cn.ichensw.system.domain.vo.OrderMemberVo;
import cn.ichensw.system.domain.vo.OrderStatisticsVo;

import java.util.List;

/**
 * 会员消费Service接口
 *
 * @author jucce
 * @date 2022-11-25
 */
public interface IOrderMemberService {
    /**
     * 查询会员消费
     *
     * @param orderMemberId 会员消费主键
     * @return 会员消费
     */
    public OrderMember selectOrderMemberByOrderMemberId(Long orderMemberId);

    /**
     * 查询会员消费列表
     *
     * @param orderMemberVo 会员消费
     * @return 会员消费集合
     */
    public List<OrderMember> selectOrderMemberList(OrderMemberVo orderMemberVo);

    /**
     * 新增会员消费
     *
     * @param orderMember 会员消费
     * @return 结果
     */
    public int insertOrderMember(OrderMember orderMember);

    /**
     * 修改会员消费
     *
     * @param orderMember 会员消费
     * @return 结果
     */
    public int updateOrderMember(OrderMember orderMember);

    /**
     * 批量删除会员消费
     *
     * @param orderMemberVos 需要删除的会员消费主键集合
     * @return 结果
     */
    public int deleteOrderMemberByOrderMemberIds(List<OrderMemberVo> orderMemberVos);

    /**
     * 删除会员消费信息
     *
     * @param orderMemberId 会员消费主键
     * @return 结果
     */
    public int deleteOrderMemberByOrderMemberId(Long orderMemberId);

    /**
     * 查询未结算的会员消费订单列表
     *
     * @return 结果
     */
    List<OrderMemberVo> selectOrderMemberListAndNoSettlement();

    /**
     * 订单结算
     *
     * @param list 订单信息集合
     * @return 结果
     */
    int updateOrderMemberByOrderMemberId(List<OrderMemberVo> list);

    /**
     * 查询订单总数
     *
     * @return 订单总数
     */
    Long selectOrderCount();

    /**
     * 查询每个月的订单总数
     *
     * @return 结果
     */
    List<OrderStatisticsVo> selectMonthOrderCount();
}
