package cn.ichensw.system.service.impl;

import cn.ichensw.system.domain.SysCarSeries;
import cn.ichensw.system.domain.vo.SysCarSeriesVo;
import cn.ichensw.system.mapper.SysCarSeriesMapper;
import cn.ichensw.system.service.ISysCarSeriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 汽车系列Service业务层处理
 *
 * @author jucce
 * @date 2022-11-26
 */
@Service
public class SysCarSeriesServiceImpl implements ISysCarSeriesService {
    @Autowired
    private SysCarSeriesMapper sysCarSeriesMapper;

    /**
     * 查询汽车系列
     *
     * @param seriesId 汽车系列主键
     * @return 汽车系列
     */
    @Override
    public SysCarSeries selectSysCarSeriesBySeriesId(Long seriesId) {
        return sysCarSeriesMapper.selectSysCarSeriesBySeriesId(seriesId);
    }

    /**
     * 查询汽车系列列表
     *
     * @param sysCarSeries 汽车系列
     * @return 汽车系列
     */
    @Override
    public List<SysCarSeriesVo> selectSysCarSeriesList(SysCarSeries sysCarSeries) {
        return sysCarSeriesMapper.selectSysCarSeriesList(sysCarSeries);
    }

    /**
     * 新增汽车系列
     *
     * @param sysCarSeries 汽车系列
     * @return 结果
     */
    @Override
    public int insertSysCarSeries(SysCarSeries sysCarSeries) {
        return sysCarSeriesMapper.insertSysCarSeries(sysCarSeries);
    }

    /**
     * 修改汽车系列
     *
     * @param sysCarSeries 汽车系列
     * @return 结果
     */
    @Override
    public int updateSysCarSeries(SysCarSeries sysCarSeries) {
        return sysCarSeriesMapper.updateSysCarSeries(sysCarSeries);
    }

    /**
     * 批量删除汽车系列
     *
     * @param seriesIds 需要删除的汽车系列主键
     * @return 结果
     */
    @Override
    public int deleteSysCarSeriesBySeriesIds(Long[] seriesIds) {
        return sysCarSeriesMapper.deleteSysCarSeriesBySeriesIds(seriesIds);
    }

    /**
     * 删除汽车系列信息
     *
     * @param seriesId 汽车系列主键
     * @return 结果
     */
    @Override
    public int deleteSysCarSeriesBySeriesId(Long seriesId) {
        return sysCarSeriesMapper.deleteSysCarSeriesBySeriesId(seriesId);
    }
}
