package cn.ichensw.system.service.impl;

import cn.ichensw.system.domain.OrderSpread;
import cn.ichensw.system.domain.StockProduct;
import cn.ichensw.system.domain.vo.OrderSpreadVo;
import cn.ichensw.system.mapper.OrderSpreadMapper;
import cn.ichensw.system.mapper.StockProductMapper;
import cn.ichensw.system.service.IOrderSpreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 散客消费Service业务层处理
 *
 * @author jucce
 * @date 2022-11-25
 */
@Service
public class OrderSpreadServiceImpl implements IOrderSpreadService {
    @Autowired
    private OrderSpreadMapper orderSpreadMapper;
    @Autowired
    private StockProductMapper stockProductMapper;

    /**
     * 查询散客消费
     *
     * @param spreadId 散客消费主键
     * @return 散客消费
     */
    @Override
    public OrderSpread selectOrderSpreadBySpreadId(Long spreadId) {
        return orderSpreadMapper.selectOrderSpreadBySpreadId(spreadId);
    }

    /**
     * 查询散客消费列表
     *
     * @param orderSpreadVo 散客消费
     * @return 散客消费
     */
    @Override
    public List<OrderSpreadVo> selectOrderSpreadList(OrderSpreadVo orderSpreadVo) {
        return orderSpreadMapper.selectOrderSpreadList(orderSpreadVo);
    }

    /**
     * 新增散客消费
     *
     * @param orderSpread 散客消费
     * @return 结果
     */
    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public int insertOrderSpread(OrderSpread orderSpread) {
        // 1. 减少库存数量
        StockProduct stockProduct = new StockProduct();
        // 产品ID
        stockProduct.setProductId(orderSpread.getProductId());
        // 需要减少的产品数量
        stockProduct.setProductCount(orderSpread.getSpreadCount());
        stockProductMapper.reduceStockProductCount(stockProduct);
        // 2. 新增散客消费记录
        return orderSpreadMapper.insertOrderSpread(orderSpread);
    }

    /**
     * 修改散客消费
     *
     * @param orderSpread 散客消费
     * @return 结果
     */
    @Override
    public int updateOrderSpread(OrderSpread orderSpread) {
        return orderSpreadMapper.updateOrderSpread(orderSpread);
    }

    /**
     * 批量删除散客消费
     *
     * @param spreadIds 需要删除的散客消费主键
     * @return 结果
     */
    @Override
    public int deleteOrderSpreadBySpreadIds(Long[] spreadIds) {
        return orderSpreadMapper.deleteOrderSpreadBySpreadIds(spreadIds);
    }

    /**
     * 删除散客消费信息
     *
     * @param spreadId 散客消费主键
     * @return 结果
     */
    @Override
    public int deleteOrderSpreadBySpreadId(Long spreadId) {
        return orderSpreadMapper.deleteOrderSpreadBySpreadId(spreadId);
    }
}
