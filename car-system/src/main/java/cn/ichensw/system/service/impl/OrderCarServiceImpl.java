package cn.ichensw.system.service.impl;

import java.util.List;

import cn.ichensw.system.domain.vo.OrderCarVO;
import org.springframework.stereotype.Service;
import cn.ichensw.system.mapper.OrderCarMapper;
import cn.ichensw.system.domain.OrderCar;
import cn.ichensw.system.service.IOrderCarService;

import javax.annotation.Resource;

/**
 * 车辆销售Service业务层处理
 *
 * @author jucce
 * @date 2023-03-05
 */
@Service
public class OrderCarServiceImpl implements IOrderCarService {
    @Resource
    private OrderCarMapper orderCarMapper;

    /**
     * 查询车辆销售
     *
     * @param orderCarId 车辆销售主键
     * @return 车辆销售
     */
    @Override
    public OrderCar selectOrderCarByOrderCarId(Long orderCarId) {
        return orderCarMapper.selectOrderCarByOrderCarId(orderCarId);
    }

    /**
     * 查询车辆销售列表
     *
     * @param orderCarVO 车辆销售
     * @return 车辆销售
     */
    @Override
    public List<OrderCarVO> selectOrderCarList(OrderCarVO orderCarVO) {
        return orderCarMapper.selectOrderCarList(orderCarVO);
    }

    /**
     * 新增车辆销售
     *
     * @param orderCar 车辆销售
     * @return 结果
     */
    @Override
    public int insertOrderCar(OrderCar orderCar) {
        return orderCarMapper.insertOrderCar(orderCar);
    }

    /**
     * 修改车辆销售
     *
     * @param orderCar 车辆销售
     * @return 结果
     */
    @Override
    public int updateOrderCar(OrderCar orderCar) {
        return orderCarMapper.updateOrderCar(orderCar);
    }

    /**
     * 批量删除车辆销售
     *
     * @param orderCarIds 需要删除的车辆销售主键
     * @return 结果
     */
    @Override
    public int deleteOrderCarByOrderCarIds(Long[] orderCarIds) {
        return orderCarMapper.deleteOrderCarByOrderCarIds(orderCarIds);
    }

    /**
     * 删除车辆销售信息
     *
     * @param orderCarId 车辆销售主键
     * @return 结果
     */
    @Override
    public int deleteOrderCarByOrderCarId(Long orderCarId) {
        return orderCarMapper.deleteOrderCarByOrderCarId(orderCarId);
    }
}
