package cn.ichensw.system.service.impl;

import cn.ichensw.system.domain.SysActivies;
import cn.ichensw.system.mapper.SysActiviesMapper;
import cn.ichensw.system.service.ISysActiviesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * 优惠活动Service业务层处理
 *
 * @author jucce
 * @date 2022-11-25
 */
@Service
public class SysActiviesServiceImpl implements ISysActiviesService {
    @Autowired
    private SysActiviesMapper sysActiviesMapper;

    /**
     * 查询优惠活动
     *
     * @param activiesId 优惠活动主键
     * @return 优惠活动
     */
    @Override
    public SysActivies selectSysActiviesByActiviesId(Long activiesId) {
        return sysActiviesMapper.selectSysActiviesByActiviesId(activiesId);
    }

    /**
     * 查询优惠活动列表
     *
     * @param sysActivies 优惠活动
     * @return 优惠活动
     */
    @Override
    public List<SysActivies> selectSysActiviesList(SysActivies sysActivies) {
        return sysActiviesMapper.selectSysActiviesList(sysActivies);
    }

    /**
     * 新增优惠活动
     *
     * @param sysActivies 优惠活动
     * @return 结果
     */
    @Override
    public int insertSysActivies(SysActivies sysActivies) {
        return sysActiviesMapper.insertSysActivies(sysActivies);
    }

    /**
     * 修改优惠活动
     *
     * @param sysActivies 优惠活动
     * @return 结果
     */
    @Override
    public int updateSysActivies(SysActivies sysActivies) {
        return sysActiviesMapper.updateSysActivies(sysActivies);
    }

    /**
     * 批量删除优惠活动
     *
     * @param activiesIds 需要删除的优惠活动主键
     * @return 结果
     */
    @Override
    public int deleteSysActiviesByActiviesIds(Long[] activiesIds) {
        return sysActiviesMapper.deleteSysActiviesByActiviesIds(activiesIds);
    }

    /**
     * 删除优惠活动信息
     *
     * @param activiesId 优惠活动主键
     * @return 结果
     */
    @Override
    public int deleteSysActiviesByActiviesId(Long activiesId) {
        return sysActiviesMapper.deleteSysActiviesByActiviesId(activiesId);
    }

    /**
     * 根据充值金额获取匹配的优惠活动信息
     *
     * @param activiesRechargeAmount 充值金额
     * @return 结果
     */
    @Override
    public List<SysActivies> selectSysActiviesByActiviesRechargeAmount(BigDecimal activiesRechargeAmount) {
        return sysActiviesMapper.selectSysActiviesByActiviesRechargeAmount(activiesRechargeAmount);
    }
}
