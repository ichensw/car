package cn.ichensw.system.service;

import cn.ichensw.system.domain.SysProductType;

import java.util.List;

/**
 * 产品类别Service接口
 *
 * @author jucce
 * @date 2022-11-25
 */
public interface ISysProductTypeService {
    /**
     * 查询产品类别
     *
     * @param typeId 产品类别主键
     * @return 产品类别
     */
    public SysProductType selectSysProductTypeByTypeId(Long typeId);

    /**
     * 查询产品类别列表
     *
     * @param sysProductType 产品类别
     * @return 产品类别集合
     */
    public List<SysProductType> selectSysProductTypeList(SysProductType sysProductType);

    /**
     * 新增产品类别
     *
     * @param sysProductType 产品类别
     * @return 结果
     */
    public int insertSysProductType(SysProductType sysProductType);

    /**
     * 修改产品类别
     *
     * @param sysProductType 产品类别
     * @return 结果
     */
    public int updateSysProductType(SysProductType sysProductType);

    /**
     * 批量删除产品类别
     *
     * @param typeIds 需要删除的产品类别主键集合
     * @return 结果
     */
    public int deleteSysProductTypeByTypeIds(Long[] typeIds);

    /**
     * 删除产品类别信息
     *
     * @param typeId 产品类别主键
     * @return 结果
     */
    public int deleteSysProductTypeByTypeId(Long typeId);
}
