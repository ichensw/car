package cn.ichensw.system.service.impl;

import cn.ichensw.system.domain.SysGrade;
import cn.ichensw.system.domain.vo.SysGradeVo;
import cn.ichensw.system.mapper.SysGradeMapper;
import cn.ichensw.system.service.ISysGradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会员等级Service业务层处理
 *
 * @author jucce
 * @date 2022-11-25
 */
@Service
public class SysGradeServiceImpl implements ISysGradeService {
    @Autowired
    private SysGradeMapper sysGradeMapper;

    /**
     * 查询会员等级
     *
     * @param gradeId 会员等级主键
     * @return 会员等级
     */
    @Override
    public SysGrade selectSysGradeByGradeId(Long gradeId) {
        return sysGradeMapper.selectSysGradeByGradeId(gradeId);
    }

    /**
     * 查询会员等级列表
     *
     * @param sysGrade 会员等级
     * @return 会员等级
     */
    @Override
    public List<SysGrade> selectSysGradeList(SysGrade sysGrade) {
        return sysGradeMapper.selectSysGradeList(sysGrade);
    }

    /**
     * 新增会员等级
     *
     * @param sysGrade 会员等级
     * @return 结果
     */
    @Override
    public int insertSysGrade(SysGrade sysGrade) {
        return sysGradeMapper.insertSysGrade(sysGrade);
    }

    /**
     * 修改会员等级
     *
     * @param sysGrade 会员等级
     * @return 结果
     */
    @Override
    public int updateSysGrade(SysGrade sysGrade) {
        return sysGradeMapper.updateSysGrade(sysGrade);
    }

    /**
     * 批量删除会员等级
     *
     * @param gradeIds 需要删除的会员等级主键
     * @return 结果
     */
    @Override
    public int deleteSysGradeByGradeIds(Long[] gradeIds) {
        return sysGradeMapper.deleteSysGradeByGradeIds(gradeIds);
    }

    /**
     * 删除会员等级信息
     *
     * @param gradeId 会员等级主键
     * @return 结果
     */
    @Override
    public int deleteSysGradeByGradeId(Long gradeId) {
        return sysGradeMapper.deleteSysGradeByGradeId(gradeId);
    }

    /**
     * 查询会员等级 和 各等级对应人数
     *
     * @return 结果
     */
    @Override
    public List<SysGradeVo> selectSysGradeAndMemberNumber() {
        return sysGradeMapper.selectSysGradeAndMemberNumber();
    }
}
