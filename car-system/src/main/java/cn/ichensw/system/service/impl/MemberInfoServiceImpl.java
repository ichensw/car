package cn.ichensw.system.service.impl;

import cn.ichensw.system.domain.MemberInfo;
import cn.ichensw.system.domain.vo.MemberInfoVo;
import cn.ichensw.system.domain.vo.UserStatisticsVo;
import cn.ichensw.system.mapper.MemberInfoMapper;
import cn.ichensw.system.service.IMemberInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * 会员信息Service业务层处理
 *
 * @author jucce
 * @date 2022-11-25
 */
@Service
public class MemberInfoServiceImpl implements IMemberInfoService {
    @Autowired
    private MemberInfoMapper memberInfoMapper;

    /**
     * 查询会员信息
     *
     * @param memberInfoId 会员信息主键
     * @return 会员信息
     */
    @Override
    public MemberInfo selectMemberInfoByMemberInfoId(Long memberInfoId) {
        return memberInfoMapper.selectMemberInfoByMemberInfoId(memberInfoId);
    }

    /**
     * 查询会员信息列表
     *
     * @param memberInfo 会员信息
     * @return 会员信息
     */
    @Override
    public List<MemberInfo> selectMemberInfoList(MemberInfo memberInfo) {
        return memberInfoMapper.selectMemberInfoList(memberInfo);
    }

    /**
     * 新增会员信息
     *
     * @param memberInfo 会员信息
     * @return 结果
     */
    @Override
    public int insertMemberInfo(MemberInfo memberInfo) {
        /* 初始化 */
        // 生成会员卡号
        String number = generateNumber();

        // 会员卡号
        memberInfo.setMemberInfoNumber(number);
        // 会员余额
        memberInfo.setMemberInfoMoney(new BigDecimal("0"));
        // 会员积分
        memberInfo.setMemberInfoIntegral(0L);
        // 会员等级:默认青铜会员
        memberInfo.setGradeId(1L);
        // 会员开通时间
        memberInfo.setMemberInfoCreateTime(new Date());
        return memberInfoMapper.insertMemberInfo(memberInfo);
    }

    /**
     * 生成会员卡号
     *
     * @return 会员卡号
     */
    private String generateNumber() {
        // 前缀随机数
        int randomNumPrefix = new Random().nextInt(100);
        // 会员卡号
        StringBuilder cardNumber = new StringBuilder(String.valueOf(randomNumPrefix));
        // 后缀随机数
        int randomNumSuffix = new Random().nextInt(10000);
        // 年月日
        SimpleDateFormat sfd = new SimpleDateFormat("yyyyMMdd");
        String dateStr = sfd.format(new Date());

        cardNumber.append(dateStr);
        cardNumber.append(randomNumSuffix);
        return cardNumber.toString();
    }

    /**
     * 修改会员信息
     *
     * @param memberInfo 会员信息
     * @return 结果
     */
    @Override
    public int updateMemberInfo(MemberInfo memberInfo) {
        return memberInfoMapper.updateMemberInfo(memberInfo);
    }

    /**
     * 批量删除会员信息
     *
     * @param memberInfoIds 需要删除的会员信息主键
     * @return 结果
     */
    @Override
    public int deleteMemberInfoByMemberInfoIds(Long[] memberInfoIds) {
        return memberInfoMapper.deleteMemberInfoByMemberInfoIds(memberInfoIds);
    }

    /**
     * 删除会员信息信息
     *
     * @param memberInfoId 会员信息主键
     * @return 结果
     */
    @Override
    public int deleteMemberInfoByMemberInfoId(Long memberInfoId) {
        return memberInfoMapper.deleteMemberInfoByMemberInfoId(memberInfoId);
    }

    /**
     * 通过会员卡号查询会员信息
     *
     * @param memberInfoNumber 会员卡号
     * @return 结果
     */
    @Override
    public String selectMemberIdByMemberInfoNumber(String memberInfoNumber) {
        return memberInfoMapper.selectMemberIdByMemberInfoNumber(memberInfoNumber);
    }

    /**
     * 根据卡号查询会员详细信息
     *
     * @param memberInfoNumber 会员卡号
     * @return 会员详细信息
     */
    @Override
    public MemberInfoVo selectMemberInfoByMemberInfoNumber(String memberInfoNumber) {
        return memberInfoMapper.selectMemberInfoByMemberInfoNumber(memberInfoNumber);
    }

    /**
     * 获取用户总数
     *
     * @return 用户总数
     */
    @Override
    public Long selectUserCount() {
        return memberInfoMapper.selectUserCount();
    }

    /**
     * 查询每个月的会员注册人数
     *
     * @return 结果
     */
    @Override
    public List<UserStatisticsVo> selectMonthUserCount() {
        return memberInfoMapper.selectMonthUserCount();
    }
}
