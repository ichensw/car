package cn.ichensw.system.service.impl;

import cn.ichensw.system.domain.StockManage;
import cn.ichensw.system.domain.StockProduct;
import cn.ichensw.system.domain.vo.StockManageVo;
import cn.ichensw.system.mapper.StockManageMapper;
import cn.ichensw.system.mapper.StockProductMapper;
import cn.ichensw.system.service.IStockManageService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 库存管理Service业务层处理
 *
 * @author jucce
 * @date 2022-11-25
 */
@Service
public class StockManageServiceImpl implements IStockManageService {
    @Autowired
    private StockManageMapper stockManageMapper;

    @Autowired
    private StockProductMapper stockProductMapper;

    /**
     * 查询库存管理
     *
     * @param manageId 库存管理主键
     * @return 库存管理
     */
    @Override
    public StockManage selectStockManageByManageId(Long manageId) {
        return stockManageMapper.selectStockManageByManageId(manageId);
    }

    /**
     * 查询库存管理列表
     *
     * @param stockManage 库存管理
     * @return 库存管理
     */
    @Override
    public List<StockManageVo> selectStockManageList(StockManage stockManage) {
        return stockManageMapper.selectStockManageList(stockManage);
    }

    /**
     * 新增库存管理
     *
     * @param stockManage 库存管理
     * @return 结果
     */
    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public int insertStockManage(StockManage stockManage) {
        // 根据产品ID获取产品类别ID
        StockProduct stockProduct = stockProductMapper.selectStockProductByProductId(stockManage.getProductId());
        stockManage.setTypeId(stockProduct.getTypeId());
        int i = stockManageMapper.insertStockManage(stockManage);
        // 修改产品信息数量
        stockProduct.setProductCount(stockManage.getManageCount());
        i = stockProductMapper.updateStockProduct(stockProduct);
        return i;
    }

    /**
     * 修改库存管理
     *
     * @param stockManage 库存管理
     * @return 结果
     */
    @Override
    public int updateStockManage(StockManage stockManage) {
        return stockManageMapper.updateStockManage(stockManage);
    }

    /**
     * 批量删除库存管理
     *
     * @param manageIds 需要删除的库存管理主键
     * @return 结果
     */
    @Override
    public int deleteStockManageByManageIds(Long[] manageIds) {
        return stockManageMapper.deleteStockManageByManageIds(manageIds);
    }

    /**
     * 删除库存管理信息
     *
     * @param manageId 库存管理主键
     * @return 结果
     */
    @Override
    public int deleteStockManageByManageId(Long manageId) {
        return stockManageMapper.deleteStockManageByManageId(manageId);
    }
}
