package cn.ichensw.system.service.impl;

import cn.ichensw.common.utils.PageUtils;
import cn.ichensw.common.utils.StringUtils;
import cn.ichensw.system.domain.MemberInfo;
import cn.ichensw.system.domain.MemberRecharge;
import cn.ichensw.system.domain.SysActivies;
import cn.ichensw.system.domain.vo.MemberRechargeVo;
import cn.ichensw.system.mapper.MemberInfoMapper;
import cn.ichensw.system.mapper.MemberRechargeMapper;
import cn.ichensw.system.mapper.SysActiviesMapper;
import cn.ichensw.system.mapper.SysUserMapper;
import cn.ichensw.system.service.IMemberRechargeService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 会员充值Service业务层处理
 *
 * @author jucce
 * @date 2022-11-25
 */
@Service
public class MemberRechargeServiceImpl implements IMemberRechargeService {
    @Autowired
    private MemberRechargeMapper memberRechargeMapper;
    @Autowired
    private MemberInfoMapper memberInfoMapper;

    /**
     * 查询会员充值
     *
     * @param rechargeId 会员充值主键
     * @return 会员充值
     */
    @Override
    public MemberRecharge selectMemberRechargeByRechargeId(Long rechargeId) {
        return memberRechargeMapper.selectMemberRechargeByRechargeId(rechargeId);
    }

    /**
     * 查询会员充值列表
     *
     * @param memberRechargeVo 会员充值
     * @return 会员充值
     */
    @Override
    public List<MemberRechargeVo> selectMemberRechargeList(MemberRechargeVo memberRechargeVo) {
        // 查询充值记录
        return memberRechargeMapper.selectMemberRechargeList(memberRechargeVo);
    }

    /**
     * 新增会员充值
     *
     * @param memberRecharge 会员充值
     * @return 结果
     */
    @Override
    public int insertMemberRecharge(MemberRecharge memberRecharge) {
        int flag = memberRechargeMapper.insertMemberRecharge(memberRecharge);
        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setMemberInfoId(memberRecharge.getMemberInfoId());
        // 计算充值余额
        BigDecimal memberInfoMoney = memberRecharge.getRechargeAmount();
        if (memberRecharge.getRechargeGiveAmount() != null) {
            memberInfoMoney = memberInfoMoney.add(memberRecharge.getRechargeGiveAmount());
        }
        memberInfo.setMemberInfoMoney(memberInfoMoney);
        memberInfoMapper.addMemberInfoMoney(memberInfo);
        return flag;
    }

    /**
     * 修改会员充值
     *
     * @param memberRecharge 会员充值
     * @return 结果
     */
    @Override
    public int updateMemberRecharge(MemberRecharge memberRecharge) {
        return memberRechargeMapper.updateMemberRecharge(memberRecharge);
    }

    /**
     * 批量删除会员充值
     *
     * @param rechargeIds 需要删除的会员充值主键
     * @return 结果
     */
    @Override
    public int deleteMemberRechargeByRechargeIds(Long[] rechargeIds) {
        return memberRechargeMapper.deleteMemberRechargeByRechargeIds(rechargeIds);
    }

    /**
     * 删除会员充值信息
     *
     * @param rechargeId 会员充值主键
     * @return 结果
     */
    @Override
    public int deleteMemberRechargeByRechargeId(Long rechargeId) {
        return memberRechargeMapper.deleteMemberRechargeByRechargeId(rechargeId);
    }

    /**
     * 校验会员支付密码是否正确
     *
     * @param memberInfo 会员信息
     * @return 结果
     */
    @Override
    public boolean checkNumberAndPassword(MemberInfo memberInfo) {
        return memberRechargeMapper.checkNumberAndPassword(memberInfo) != 0;
    }

}
