package cn.ichensw.system.service.impl;

import cn.ichensw.system.domain.OrderMember;
import cn.ichensw.system.domain.StockProduct;
import cn.ichensw.system.domain.dto.MemberInfoDTO;
import cn.ichensw.system.domain.vo.OrderMemberVo;
import cn.ichensw.system.domain.vo.OrderStatisticsVo;
import cn.ichensw.system.mapper.MemberInfoMapper;
import cn.ichensw.system.mapper.OrderMemberMapper;
import cn.ichensw.system.mapper.StockProductMapper;
import cn.ichensw.system.mapper.SysGradeMapper;
import cn.ichensw.system.service.IOrderMemberService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 会员消费Service业务层处理
 *
 * @author jucce
 * @date 2022-11-25
 */
@Service
public class OrderMemberServiceImpl implements IOrderMemberService {
    @Resource
    private OrderMemberMapper orderMemberMapper;
    @Resource
    private StockProductMapper stockProductMapper;
    @Resource
    private MemberInfoMapper memberInfoMapper;
    @Resource
    private SysGradeMapper sysGradeMapper;

    /**
     * 查询会员消费
     *
     * @param orderMemberId 会员消费主键
     * @return 会员消费
     */
    @Override
    public OrderMember selectOrderMemberByOrderMemberId(Long orderMemberId) {
        return orderMemberMapper.selectOrderMemberByOrderMemberId(orderMemberId);
    }

    /**
     * 查询会员消费列表
     *
     * @param orderMemberVo 会员消费
     * @return 会员消费
     */
    @Override
    public List<OrderMember> selectOrderMemberList(OrderMemberVo orderMemberVo) {
        return orderMemberMapper.selectOrderMemberList(orderMemberVo);
    }

    /**
     * 新增会员消费
     * 1. 修改库存数量
     * 2. 新增会员消费记录
     *
     * @param orderMember 会员消费
     * @return 结果
     */
    @Override
    public int insertOrderMember(OrderMember orderMember) {
        // 1.修改库存数量
        StockProduct stockProduct = new StockProduct();
        stockProduct.setProductId(orderMember.getProductId());
        stockProduct.setProductCount(orderMember.getOrderMemberCount());
        stockProductMapper.reduceStockProductCount(stockProduct);

        // 2.新增会员消费记录
        return orderMemberMapper.insertOrderMember(orderMember);
    }

    /**
     * 修改会员消费
     *
     * @param orderMember 会员消费
     * @return 结果
     */
    @Override
    public int updateOrderMember(OrderMember orderMember) {
        return orderMemberMapper.updateOrderMember(orderMember);
    }

    /**
     * 批量删除会员消费
     * 1. 回滚商品库存数量
     * 2. 删除会员消费记录
     *
     * @param orderMemberVos 需要删除的会员消费主键
     * @return 结果
     */
    @Override
    public int deleteOrderMemberByOrderMemberIds(List<OrderMemberVo> orderMemberVos) {
        AtomicInteger flag = new AtomicInteger();
        // 1. 回滚商品库存数量
        orderMemberVos.forEach(item -> {
            StockProduct stockProduct = new StockProduct();
            // 产品ID
            stockProduct.setProductId(item.getProductId());
            // 购买数量
            stockProduct.setProductCount(item.getOrderMemberCount());
            flag.set(stockProductMapper.addStockProductCount(stockProduct));
            // 2. 删除会员消费记录
            flag.set(orderMemberMapper.deleteOrderMemberByOrderMemberId(item.getOrderMemberId()));
        });
        return flag.get();
    }

    /**
     * 删除会员消费信息
     *
     * @param orderMemberId 会员消费主键
     * @return 结果
     */
    @Override
    public int deleteOrderMemberByOrderMemberId(Long orderMemberId) {
        return orderMemberMapper.deleteOrderMemberByOrderMemberId(orderMemberId);
    }

    /**
     * 查询未结算的会员消费订单列表
     *
     * @return 结果
     */
    @Override
    public List<OrderMemberVo> selectOrderMemberListAndNoSettlement() {
        return orderMemberMapper.selectOrderMemberListAndNoSettlement();
    }

    /**
     * 订单结算--结算传入的订单
     *
     * @param list 订单信息集合
     * @return 结果
     */
    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public int updateOrderMemberByOrderMemberId(List<OrderMemberVo> list) {
        AtomicInteger flag = new AtomicInteger();
        list.forEach(item -> {
            // 更改订单状态
            flag.set(orderMemberMapper.updateOrderMemberByOne(item.getOrderMemberId()));
        });
        // 3. 根据会员卡号--修改会员余额 并且 1:1新增会员积分
        OrderMemberVo orderMemberVo = list.get(0);
        // 设置会员积分
        orderMemberVo.setMemberInfoIntegral(orderMemberVo.getMemberInfoMoney().longValue());
        flag.set(memberInfoMapper.updateMemberInfoMoney(orderMemberVo));
        // 查询积分
        MemberInfoDTO memberInfoDTO = memberInfoMapper.selectMemberInfoDTOByMemberInfoId(orderMemberVo.getMemberInfoNumber());
        // 判断积分级别
        Long gradeId = sysGradeMapper.selectSysGradeByGradeIntegral(memberInfoDTO.getMemberInfoIntegral());
        // 修改会员等级
        memberInfoDTO.setGradeId(gradeId);
        flag.set(memberInfoMapper.updateMemberInfoGrade(memberInfoDTO));
        return flag.get();
    }


    /**
     * 查询订单总数
     *
     * @return 订单总数
     */
    @Override
    public Long selectOrderCount() {
        return memberInfoMapper.selectOrderCount();
    }

    /**
     * 查询每个月的订单总数
     *
     * @return 结果
     */
    @Override
    public List<OrderStatisticsVo> selectMonthOrderCount() {
        return orderMemberMapper.selectMonthOrderCount();
    }

}
