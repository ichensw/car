package cn.ichensw.system.service;

import java.util.List;
import cn.ichensw.system.domain.OrderCar;
import cn.ichensw.system.domain.vo.OrderCarVO;

/**
 * 车辆销售Service接口
 * 
 * @author jucce
 * @date 2023-03-05
 */
public interface IOrderCarService 
{
    /**
     * 查询车辆销售
     * 
     * @param orderCarId 车辆销售主键
     * @return 车辆销售
     */
    public OrderCar selectOrderCarByOrderCarId(Long orderCarId);

    /**
     * 查询车辆销售列表
     *
     * @param orderCarVO 车辆销售
     * @return 车辆销售集合
     */
    public List<OrderCarVO> selectOrderCarList(OrderCarVO orderCarVO);

    /**
     * 新增车辆销售
     * 
     * @param orderCar 车辆销售
     * @return 结果
     */
    public int insertOrderCar(OrderCar orderCar);

    /**
     * 修改车辆销售
     * 
     * @param orderCar 车辆销售
     * @return 结果
     */
    public int updateOrderCar(OrderCar orderCar);

    /**
     * 批量删除车辆销售
     * 
     * @param orderCarIds 需要删除的车辆销售主键集合
     * @return 结果
     */
    public int deleteOrderCarByOrderCarIds(Long[] orderCarIds);

    /**
     * 删除车辆销售信息
     * 
     * @param orderCarId 车辆销售主键
     * @return 结果
     */
    public int deleteOrderCarByOrderCarId(Long orderCarId);
}
