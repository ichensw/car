package cn.ichensw.system.service;

import cn.ichensw.system.domain.MemberInfo;
import cn.ichensw.system.domain.vo.MemberInfoVo;
import cn.ichensw.system.domain.vo.UserStatisticsVo;

import java.util.ArrayList;
import java.util.List;

/**
 * 会员信息Service接口
 *
 * @author jucce
 * @date 2022-11-25
 */
public interface IMemberInfoService {
    /**
     * 查询会员信息
     *
     * @param memberInfoId 会员信息主键
     * @return 会员信息
     */
    public MemberInfo selectMemberInfoByMemberInfoId(Long memberInfoId);

    /**
     * 查询会员信息列表
     *
     * @param memberInfo 会员信息
     * @return 会员信息集合
     */
    public List<MemberInfo> selectMemberInfoList(MemberInfo memberInfo);

    /**
     * 新增会员信息
     *
     * @param memberInfo 会员信息
     * @return 结果
     */
    public int insertMemberInfo(MemberInfo memberInfo);

    /**
     * 修改会员信息
     *
     * @param memberInfo 会员信息
     * @return 结果
     */
    public int updateMemberInfo(MemberInfo memberInfo);

    /**
     * 批量删除会员信息
     *
     * @param memberInfoIds 需要删除的会员信息主键集合
     * @return 结果
     */
    public int deleteMemberInfoByMemberInfoIds(Long[] memberInfoIds);

    /**
     * 删除会员信息
     *
     * @param memberInfoId 会员信息主键
     * @return 结果
     */
    public int deleteMemberInfoByMemberInfoId(Long memberInfoId);

    /**
     * 通过会员卡号查询会员信息
     *
     * @param memberInfoNumber 会员卡号
     * @return 结果
     */
    String selectMemberIdByMemberInfoNumber(String memberInfoNumber);

    /**
     * 根据卡号查询会员详细信息
     *
     * @param memberInfoNumber 会员卡号
     * @return 会员详细信息
     */
    MemberInfoVo selectMemberInfoByMemberInfoNumber(String memberInfoNumber);

    /**
     * 获取用户总数
     *
     * @return 用户总数
     */
    Long selectUserCount();

    /**
     * 查询每个月的会员注册人数
     *
     * @return 结果
     */
    List<UserStatisticsVo> selectMonthUserCount();
}
