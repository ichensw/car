package cn.ichensw.system.service.impl;

import cn.ichensw.system.domain.SysProductType;
import cn.ichensw.system.mapper.SysProductTypeMapper;
import cn.ichensw.system.service.ISysProductTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 产品类别Service业务层处理
 *
 * @author jucce
 * @date 2022-11-25
 */
@Service
public class SysProductTypeServiceImpl implements ISysProductTypeService {
    @Autowired
    private SysProductTypeMapper sysProductTypeMapper;

    /**
     * 查询产品类别
     *
     * @param typeId 产品类别主键
     * @return 产品类别
     */
    @Override
    public SysProductType selectSysProductTypeByTypeId(Long typeId) {
        return sysProductTypeMapper.selectSysProductTypeByTypeId(typeId);
    }

    /**
     * 查询产品类别列表
     *
     * @param sysProductType 产品类别
     * @return 产品类别
     */
    @Override
    public List<SysProductType> selectSysProductTypeList(SysProductType sysProductType) {
        return sysProductTypeMapper.selectSysProductTypeList(sysProductType);
    }

    /**
     * 新增产品类别
     *
     * @param sysProductType 产品类别
     * @return 结果
     */
    @Override
    public int insertSysProductType(SysProductType sysProductType) {
        return sysProductTypeMapper.insertSysProductType(sysProductType);
    }

    /**
     * 修改产品类别
     *
     * @param sysProductType 产品类别
     * @return 结果
     */
    @Override
    public int updateSysProductType(SysProductType sysProductType) {
        return sysProductTypeMapper.updateSysProductType(sysProductType);
    }

    /**
     * 批量删除产品类别
     *
     * @param typeIds 需要删除的产品类别主键
     * @return 结果
     */
    @Override
    public int deleteSysProductTypeByTypeIds(Long[] typeIds) {
        return sysProductTypeMapper.deleteSysProductTypeByTypeIds(typeIds);
    }

    /**
     * 删除产品类别信息
     *
     * @param typeId 产品类别主键
     * @return 结果
     */
    @Override
    public int deleteSysProductTypeByTypeId(Long typeId) {
        return sysProductTypeMapper.deleteSysProductTypeByTypeId(typeId);
    }
}
