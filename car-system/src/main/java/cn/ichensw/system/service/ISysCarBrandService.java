package cn.ichensw.system.service;

import cn.ichensw.system.domain.SysCarBrand;
import cn.ichensw.system.domain.vo.SysCarBrandVO;

import java.util.List;

/**
 * 汽车品牌Service接口
 *
 * @author jucce
 * @date 2022-11-26
 */
public interface ISysCarBrandService {
    /**
     * 查询汽车品牌
     *
     * @param brandId 汽车品牌主键
     * @return 汽车品牌
     */
    public SysCarBrand selectSysCarBrandByBrandId(Long brandId);

    /**
     * 查询汽车品牌列表
     *
     * @param sysCarBrand 汽车品牌
     * @return 汽车品牌集合
     */
    public List<SysCarBrand> selectSysCarBrandList(SysCarBrand sysCarBrand);

    /**
     * 新增汽车品牌
     *
     * @param sysCarBrand 汽车品牌
     * @return 结果
     */
    public int insertSysCarBrand(SysCarBrand sysCarBrand);

    /**
     * 修改汽车品牌
     *
     * @param sysCarBrand 汽车品牌
     * @return 结果
     */
    public int updateSysCarBrand(SysCarBrand sysCarBrand);

    /**
     * 批量删除汽车品牌
     *
     * @param brandIds 需要删除的汽车品牌主键集合
     * @return 结果
     */
    public int deleteSysCarBrandByBrandIds(Long[] brandIds);

    /**
     * 删除汽车品牌信息
     *
     * @param brandId 汽车品牌主键
     * @return 结果
     */
    public int deleteSysCarBrandByBrandId(Long brandId);
}
