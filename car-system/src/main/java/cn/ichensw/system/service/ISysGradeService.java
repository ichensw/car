package cn.ichensw.system.service;

import cn.ichensw.system.domain.SysGrade;
import cn.ichensw.system.domain.vo.SysGradeVo;

import java.util.List;

/**
 * 会员等级Service接口
 *
 * @author jucce
 * @date 2022-11-25
 */
public interface ISysGradeService {
    /**
     * 查询会员等级
     *
     * @param gradeId 会员等级主键
     * @return 会员等级
     */
    public SysGrade selectSysGradeByGradeId(Long gradeId);

    /**
     * 查询会员等级列表
     *
     * @param sysGrade 会员等级
     * @return 会员等级集合
     */
    public List<SysGrade> selectSysGradeList(SysGrade sysGrade);

    /**
     * 新增会员等级
     *
     * @param sysGrade 会员等级
     * @return 结果
     */
    public int insertSysGrade(SysGrade sysGrade);

    /**
     * 修改会员等级
     *
     * @param sysGrade 会员等级
     * @return 结果
     */
    public int updateSysGrade(SysGrade sysGrade);

    /**
     * 批量删除会员等级
     *
     * @param gradeIds 需要删除的会员等级主键集合
     * @return 结果
     */
    public int deleteSysGradeByGradeIds(Long[] gradeIds);

    /**
     * 删除会员等级信息
     *
     * @param gradeId 会员等级主键
     * @return 结果
     */
    public int deleteSysGradeByGradeId(Long gradeId);

    /**
     * 查询会员等级 和 各等级对应人数
     *
     * @return 结果
     */
    List<SysGradeVo> selectSysGradeAndMemberNumber();

}
