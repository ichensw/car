package cn.ichensw.system.service;

import cn.ichensw.system.domain.SysActivies;

import java.math.BigDecimal;
import java.util.List;

/**
 * 优惠活动Service接口
 *
 * @author jucce
 * @date 2022-11-25
 */
public interface ISysActiviesService {
    /**
     * 查询优惠活动
     *
     * @param activiesId 优惠活动主键
     * @return 优惠活动
     */
    public SysActivies selectSysActiviesByActiviesId(Long activiesId);

    /**
     * 查询优惠活动列表
     *
     * @param sysActivies 优惠活动
     * @return 优惠活动集合
     */
    public List<SysActivies> selectSysActiviesList(SysActivies sysActivies);

    /**
     * 新增优惠活动
     *
     * @param sysActivies 优惠活动
     * @return 结果
     */
    public int insertSysActivies(SysActivies sysActivies);

    /**
     * 修改优惠活动
     *
     * @param sysActivies 优惠活动
     * @return 结果
     */
    public int updateSysActivies(SysActivies sysActivies);

    /**
     * 批量删除优惠活动
     *
     * @param activiesIds 需要删除的优惠活动主键集合
     * @return 结果
     */
    public int deleteSysActiviesByActiviesIds(Long[] activiesIds);

    /**
     * 删除优惠活动信息
     *
     * @param activiesId 优惠活动主键
     * @return 结果
     */
    public int deleteSysActiviesByActiviesId(Long activiesId);

    /**
     * 根据充值金额获取匹配的优惠活动信息
     *
     * @param activiesRechargeAmount 充值金额
     * @return 结果
     */
    List<SysActivies> selectSysActiviesByActiviesRechargeAmount(BigDecimal activiesRechargeAmount);
}
