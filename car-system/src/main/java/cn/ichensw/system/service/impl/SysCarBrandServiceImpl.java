package cn.ichensw.system.service.impl;

import cn.ichensw.common.enums.ErrorCode;
import cn.ichensw.common.exception.GlobalException;
import cn.ichensw.common.exception.ServiceException;
import cn.ichensw.system.domain.SysCarBrand;
import cn.ichensw.system.domain.vo.SysCarBrandVO;
import cn.ichensw.system.mapper.SysCarBrandMapper;
import cn.ichensw.system.service.ISysCarBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 汽车品牌Service业务层处理
 *
 * @author jucce
 * @date 2022-11-26
 */
@Service
public class SysCarBrandServiceImpl implements ISysCarBrandService {
    @Autowired
    private SysCarBrandMapper sysCarBrandMapper;

    /**
     * 查询汽车品牌
     *
     * @param brandId 汽车品牌主键
     * @return 汽车品牌
     */
    @Override
    public SysCarBrand selectSysCarBrandByBrandId(Long brandId) {
        return sysCarBrandMapper.selectSysCarBrandByBrandId(brandId);
    }

    /**
     * 查询汽车品牌列表
     *
     * @param sysCarBrand 汽车品牌
     * @return 汽车品牌
     */
    @Override
    public List<SysCarBrand> selectSysCarBrandList(SysCarBrand sysCarBrand) {
        return sysCarBrandMapper.selectSysCarBrandList(sysCarBrand);
    }

    /**
     * 新增汽车品牌
     *
     * @param sysCarBrand 汽车品牌
     * @return 结果
     */
    @Override
    public int insertSysCarBrand(SysCarBrand sysCarBrand) {
        return sysCarBrandMapper.insertSysCarBrand(sysCarBrand);
    }

    /**
     * 修改汽车品牌
     *
     * @param sysCarBrand 汽车品牌
     * @return 结果
     */
    @Override
    public int updateSysCarBrand(SysCarBrand sysCarBrand) {
        return sysCarBrandMapper.updateSysCarBrand(sysCarBrand);
    }

    /**
     * 批量删除汽车品牌
     *
     * @param brandIds 需要删除的汽车品牌主键
     * @return 结果
     */
    @Override
    public int deleteSysCarBrandByBrandIds(Long[] brandIds) {
        return sysCarBrandMapper.deleteSysCarBrandByBrandIds(brandIds);
    }

    /**
     * 删除汽车品牌信息
     *
     * @param brandId 汽车品牌主键
     * @return 结果
     */
    @Override
    public int deleteSysCarBrandByBrandId(Long brandId) {
        return sysCarBrandMapper.deleteSysCarBrandByBrandId(brandId);
    }
}
