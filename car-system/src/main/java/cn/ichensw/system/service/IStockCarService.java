package cn.ichensw.system.service;

import java.util.List;
import cn.ichensw.system.domain.StockCar;
import cn.ichensw.system.domain.vo.StockCarVo;

/**
 * 车辆信息Service接口
 * 
 * @author jucce
 * @date 2023-03-05
 */
public interface IStockCarService 
{
    /**
     * 查询车辆信息
     * 
     * @param carId 车辆信息主键
     * @return 车辆信息
     */
    public StockCar selectStockCarByCarId(Long carId);

    /**
     * 查询车辆信息列表
     * 
     * @param stockCar 车辆信息
     * @return 车辆信息集合
     */
    public List<StockCarVo> selectStockCarList(StockCar stockCar);

    /**
     * 新增车辆信息
     * 
     * @param stockCar 车辆信息
     * @return 结果
     */
    public int insertStockCar(StockCar stockCar);

    /**
     * 修改车辆信息
     * 
     * @param stockCar 车辆信息
     * @return 结果
     */
    public int updateStockCar(StockCar stockCar);

    /**
     * 批量删除车辆信息
     * 
     * @param carIds 需要删除的车辆信息主键集合
     * @return 结果
     */
    public int deleteStockCarByCarIds(Long[] carIds);

    /**
     * 删除车辆信息信息
     * 
     * @param carId 车辆信息主键
     * @return 结果
     */
    public int deleteStockCarByCarId(Long carId);
}
