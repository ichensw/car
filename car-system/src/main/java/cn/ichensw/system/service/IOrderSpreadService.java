package cn.ichensw.system.service;

import cn.ichensw.system.domain.OrderSpread;
import cn.ichensw.system.domain.vo.OrderSpreadVo;

import java.util.List;

/**
 * 散客消费Service接口
 *
 * @author jucce
 * @date 2022-11-25
 */
public interface IOrderSpreadService {
    /**
     * 查询散客消费
     *
     * @param spreadId 散客消费主键
     * @return 散客消费
     */
    public OrderSpread selectOrderSpreadBySpreadId(Long spreadId);

    /**
     * 查询散客消费列表
     *
     * @param orderSpreadVo 散客消费
     * @return 散客消费集合
     */
    public List<OrderSpreadVo> selectOrderSpreadList(OrderSpreadVo orderSpreadVo);

    /**
     * 新增散客消费
     *
     * @param orderSpread 散客消费
     * @return 结果
     */
    public int insertOrderSpread(OrderSpread orderSpread);

    /**
     * 修改散客消费
     *
     * @param orderSpread 散客消费
     * @return 结果
     */
    public int updateOrderSpread(OrderSpread orderSpread);

    /**
     * 批量删除散客消费
     *
     * @param spreadIds 需要删除的散客消费主键集合
     * @return 结果
     */
    public int deleteOrderSpreadBySpreadIds(Long[] spreadIds);

    /**
     * 删除散客消费信息
     *
     * @param spreadId 散客消费主键
     * @return 结果
     */
    public int deleteOrderSpreadBySpreadId(Long spreadId);
}
