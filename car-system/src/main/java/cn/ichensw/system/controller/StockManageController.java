package cn.ichensw.system.controller;

import cn.ichensw.common.annotation.Log;
import cn.ichensw.common.core.controller.BaseController;
import cn.ichensw.common.core.domain.AjaxResult;
import cn.ichensw.common.core.page.TableDataInfo;
import cn.ichensw.common.enums.BusinessType;
import cn.ichensw.common.utils.poi.ExcelUtil;
import cn.ichensw.system.domain.StockManage;
import cn.ichensw.system.domain.vo.StockManageVo;
import cn.ichensw.system.service.IStockManageService;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 库存管理Controller
 *
 * @author jucce
 * @date 2022-11-25
 */
@RestController
@RequestMapping("/stock/manage")
public class StockManageController extends BaseController {
    @Autowired
    private IStockManageService stockManageService;

    /**
     * 查询库存管理列表
     */
    @GetMapping("/list")
    public TableDataInfo list(StockManage stockManage) {
        startPage();
        List<StockManageVo> list = stockManageService.selectStockManageList(stockManage);

        return getDataTable(list);
    }

    /**
     * 导出库存管理列表
     */
    @Log(title = "库存管理" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockManage stockManage) {
        List<StockManageVo> list = stockManageService.selectStockManageList(stockManage);
        ExcelUtil<StockManageVo> util = new ExcelUtil<StockManageVo>(StockManageVo.class);
        util.exportExcel(response, list, "库存管理数据");
    }

    /**
     * 获取库存管理详细信息
     */
    @GetMapping(value = "/{manageId}")
    public AjaxResult getInfo(@PathVariable("manageId") Long manageId) {
        return success(stockManageService.selectStockManageByManageId(manageId));
    }

    /**
     * 新增库存管理
     */
    @Log(title = "库存管理" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockManage stockManage) {
        return toAjax(stockManageService.insertStockManage(stockManage));
    }

    /**
     * 修改库存管理
     */
    @Log(title = "库存管理" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockManage stockManage) {
        return toAjax(stockManageService.updateStockManage(stockManage));
    }

    /**
     * 删除库存管理
     */
    @Log(title = "库存管理" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{manageIds}")
    public AjaxResult remove(@PathVariable Long[] manageIds) {
        return toAjax(stockManageService.deleteStockManageByManageIds(manageIds));
    }
}
