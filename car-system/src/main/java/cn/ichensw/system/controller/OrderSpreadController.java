package cn.ichensw.system.controller;

import cn.ichensw.common.annotation.Log;
import cn.ichensw.common.core.controller.BaseController;
import cn.ichensw.common.core.domain.AjaxResult;
import cn.ichensw.common.core.page.TableDataInfo;
import cn.ichensw.common.enums.BusinessType;
import cn.ichensw.common.utils.poi.ExcelUtil;
import cn.ichensw.system.domain.OrderSpread;
import cn.ichensw.system.domain.vo.OrderSpreadVo;
import cn.ichensw.system.service.IOrderSpreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 散客消费Controller
 *
 * @author jucce
 * @date 2022-11-25
 */
@RestController
@RequestMapping("/order/spread")
public class OrderSpreadController extends BaseController {
    @Autowired
    private IOrderSpreadService orderSpreadService;

    /**
     * 查询散客消费列表
     */
    @GetMapping("/list")
    public TableDataInfo list(OrderSpreadVo orderSpreadVo) {
        startPage();
        List<OrderSpreadVo> list = orderSpreadService.selectOrderSpreadList(orderSpreadVo);
        return getDataTable(list);
    }

    /**
     * 导出散客消费列表
     */
    @Log(title = "散客消费" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OrderSpreadVo orderSpreadVo) {
        List<OrderSpreadVo> list = orderSpreadService.selectOrderSpreadList(orderSpreadVo);
        ExcelUtil<OrderSpreadVo> util = new ExcelUtil<OrderSpreadVo>(OrderSpreadVo.class);
        util.exportExcel(response, list, "散客消费数据");
    }

    /**
     * 获取散客消费详细信息
     */
    @GetMapping(value = "/{spreadId}")
    public AjaxResult getInfo(@PathVariable("spreadId") Long spreadId) {
        return success(orderSpreadService.selectOrderSpreadBySpreadId(spreadId));
    }

    /**
     * 新增散客消费
     */
    @Log(title = "散客消费" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OrderSpread orderSpread) {
        return toAjax(orderSpreadService.insertOrderSpread(orderSpread));
    }

    /**
     * 修改散客消费
     */
    @Log(title = "散客消费" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OrderSpread orderSpread) {
        return toAjax(orderSpreadService.updateOrderSpread(orderSpread));
    }

    /**
     * 删除散客消费
     */
    @Log(title = "散客消费" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{spreadIds}")
    public AjaxResult remove(@PathVariable Long[] spreadIds) {
        return toAjax(orderSpreadService.deleteOrderSpreadBySpreadIds(spreadIds));
    }
}
