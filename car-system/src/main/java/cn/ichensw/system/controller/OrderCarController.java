package cn.ichensw.system.controller;

import cn.ichensw.common.annotation.Log;
import cn.ichensw.common.core.controller.BaseController;
import cn.ichensw.common.core.domain.AjaxResult;
import cn.ichensw.common.core.page.TableDataInfo;
import cn.ichensw.common.enums.BusinessType;
import cn.ichensw.common.utils.poi.ExcelUtil;
import cn.ichensw.system.domain.OrderCar;
import cn.ichensw.system.domain.vo.OrderCarVO;
import cn.ichensw.system.service.IOrderCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 车辆销售Controller
 *
 * @author jucce
 * @date 2023-03-05
 */
@RestController
@RequestMapping("/order/car")
public class OrderCarController extends BaseController {
    @Autowired
    private IOrderCarService orderCarService;

    /**
     * 查询车辆销售列表
     */
    @GetMapping("/list")
    public TableDataInfo list(OrderCarVO orderCarVO) {
        startPage();
        List<OrderCarVO> list = orderCarService.selectOrderCarList(orderCarVO);
        return getDataTable(list);
    }

    /**
     * 导出车辆销售列表
     */
    @Log(title = "车辆销售", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OrderCarVO orderCarVO) {
        List<OrderCarVO> list = orderCarService.selectOrderCarList(orderCarVO);
        ExcelUtil<OrderCarVO> util = new ExcelUtil<OrderCarVO>(OrderCarVO.class);
        util.exportExcel(response, list, "车辆销售数据");
    }

    /**
     * 获取车辆销售详细信息
     */
    @GetMapping(value = "/{orderCarId}")
    public AjaxResult getInfo(@PathVariable("orderCarId") Long orderCarId) {
        return AjaxResult.success(orderCarService.selectOrderCarByOrderCarId(orderCarId));
    }

    /**
     * 新增车辆销售
     */
    @Log(title = "车辆销售", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OrderCar orderCar) {
        return toAjax(orderCarService.insertOrderCar(orderCar));
    }

    /**
     * 修改车辆销售
     */
    @Log(title = "车辆销售", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OrderCar orderCar) {
        return toAjax(orderCarService.updateOrderCar(orderCar));
    }

    /**
     * 删除车辆销售
     */
    @Log(title = "车辆销售", businessType = BusinessType.DELETE)
    @DeleteMapping("/{orderCarIds}")
    public AjaxResult remove(@PathVariable Long[] orderCarIds) {
        return toAjax(orderCarService.deleteOrderCarByOrderCarIds(orderCarIds));
    }
}
