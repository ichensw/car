package cn.ichensw.system.controller;

import cn.ichensw.common.annotation.Log;
import cn.ichensw.common.core.controller.BaseController;
import cn.ichensw.common.core.domain.AjaxResult;
import cn.ichensw.common.core.page.TableDataInfo;
import cn.ichensw.common.enums.BusinessType;
import cn.ichensw.common.utils.poi.ExcelUtil;
import cn.ichensw.system.domain.MemberInfo;
import cn.ichensw.system.domain.SysActivies;
import cn.ichensw.system.service.ISysActiviesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;

/**
 * 优惠活动Controller
 *
 * @author jucce
 * @date 2022-11-25
 */
@RestController
@RequestMapping("/system/activies")
public class SysActiviesController extends BaseController {
    @Autowired
    private ISysActiviesService sysActiviesService;

    /**
     * 查询优惠活动列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysActivies sysActivies) {
        startPage();
        List<SysActivies> list = sysActiviesService.selectSysActiviesList(sysActivies);
        return getDataTable(list);
    }

    /**
     * 导出优惠活动列表
     */
    @Log(title = "优惠活动" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysActivies sysActivies) {
        List<SysActivies> list = sysActiviesService.selectSysActiviesList(sysActivies);
        ExcelUtil<SysActivies> util = new ExcelUtil<SysActivies>(SysActivies.class);
        util.exportExcel(response, list, "优惠活动数据");
    }

    /**
     * 获取优惠活动详细信息
     */
    @GetMapping(value = "/{activiesId}")
    public AjaxResult getInfo(@PathVariable("activiesId") Long activiesId) {
        return success(sysActiviesService.selectSysActiviesByActiviesId(activiesId));
    }

    /**
     * 根据充值金额获取匹配的优惠活动信息
     */
    @GetMapping(value = "/activiesRechargeAmount/{activiesRechargeAmount}")
    public AjaxResult getInfoByActiviesRechargeAmount(@PathVariable("activiesRechargeAmount") BigDecimal activiesRechargeAmount) {
        return success(sysActiviesService.selectSysActiviesByActiviesRechargeAmount(activiesRechargeAmount));
    }


    /**
     * 新增优惠活动
     */
    @Log(title = "优惠活动" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysActivies sysActivies) {
        return toAjax(sysActiviesService.insertSysActivies(sysActivies));
    }

    /**
     * 修改优惠活动
     */
    @Log(title = "优惠活动" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysActivies sysActivies) {
        return toAjax(sysActiviesService.updateSysActivies(sysActivies));
    }

    /**
     * 删除优惠活动
     */
    @Log(title = "优惠活动" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{activiesIds}")
    public AjaxResult remove(@PathVariable Long[] activiesIds) {
        return toAjax(sysActiviesService.deleteSysActiviesByActiviesIds(activiesIds));
    }
}
