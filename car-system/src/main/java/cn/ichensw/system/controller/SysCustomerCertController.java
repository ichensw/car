package cn.ichensw.system.controller;

import cn.ichensw.common.annotation.Log;
import cn.ichensw.common.core.controller.BaseController;
import cn.ichensw.common.core.domain.AjaxResult;
import cn.ichensw.common.core.page.TableDataInfo;
import cn.ichensw.common.enums.BusinessType;
import cn.ichensw.common.utils.poi.ExcelUtil;
import cn.ichensw.system.domain.SysCustomerCert;
import cn.ichensw.system.service.ISysCustomerCertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 客户凭证Controller
 *
 * @author jucce
 * @date 2022-11-26
 */
@RestController
@RequestMapping("/system/cert")
public class SysCustomerCertController extends BaseController {
    @Autowired
    private ISysCustomerCertService sysCustomerCertService;

    /**
     * 查询客户凭证列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysCustomerCert sysCustomerCert) {
        startPage();
        List<SysCustomerCert> list = sysCustomerCertService.selectSysCustomerCertList(sysCustomerCert);
        return getDataTable(list);
    }

    /**
     * 导出客户凭证列表
     */
    @Log(title = "客户凭证" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysCustomerCert sysCustomerCert) {
        List<SysCustomerCert> list = sysCustomerCertService.selectSysCustomerCertList(sysCustomerCert);
        ExcelUtil<SysCustomerCert> util = new ExcelUtil<SysCustomerCert>(SysCustomerCert.class);
        util.exportExcel(response, list, "客户凭证数据");
    }

    /**
     * 获取客户凭证详细信息
     */
    @GetMapping(value = "/{certId}")
    public AjaxResult getInfo(@PathVariable("certId") Long certId) {
        return success(sysCustomerCertService.selectSysCustomerCertByCertId(certId));
    }

    /**
     * 新增客户凭证
     */
    @Log(title = "客户凭证" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysCustomerCert sysCustomerCert) {
        return toAjax(sysCustomerCertService.insertSysCustomerCert(sysCustomerCert));
    }

    /**
     * 修改客户凭证
     */
    @Log(title = "客户凭证" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysCustomerCert sysCustomerCert) {
        return toAjax(sysCustomerCertService.updateSysCustomerCert(sysCustomerCert));
    }

    /**
     * 删除客户凭证
     */
    @Log(title = "客户凭证" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{certIds}")
    public AjaxResult remove(@PathVariable Long[] certIds) {
        return toAjax(sysCustomerCertService.deleteSysCustomerCertByCertIds(certIds));
    }
}
