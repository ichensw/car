package cn.ichensw.system.controller;

import cn.ichensw.common.annotation.Log;
import cn.ichensw.common.core.controller.BaseController;
import cn.ichensw.common.core.domain.AjaxResult;
import cn.ichensw.common.core.page.TableDataInfo;
import cn.ichensw.common.enums.BusinessType;
import cn.ichensw.common.utils.poi.ExcelUtil;
import cn.ichensw.system.domain.MemberInfo;
import cn.ichensw.system.domain.vo.UserStatisticsVo;
import cn.ichensw.system.service.IMemberInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 会员信息Controller
 *
 * @author jucce
 * @date 2022-11-25
 */
@RestController
@RequestMapping("/member/info")
public class MemberInfoController extends BaseController {
    @Autowired
    private IMemberInfoService memberInfoService;

    /**
     * 查询会员信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(MemberInfo memberInfo) {
        startPage();
        List<MemberInfo> list = memberInfoService.selectMemberInfoList(memberInfo);
        return getDataTable(list);
    }

    /**
     * 获取用户总数
     */
    @GetMapping("/getUserCount")
    public AjaxResult getUserCount() {
        Long userCount = memberInfoService.selectUserCount();
        return success().put("data", userCount);
    }

    /**
     * 查询每个月的会员注册人数
     */
    @GetMapping("/getMonthUserCount")
    public AjaxResult getMonthUserCount() {
        List<UserStatisticsVo> userCountByMonth = memberInfoService.selectMonthUserCount();
        return success().put("data", userCountByMonth);
    }




    /**
     * 导出会员信息列表
     */
    @Log(title = "会员信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MemberInfo memberInfo) {
        List<MemberInfo> list = memberInfoService.selectMemberInfoList(memberInfo);
        ExcelUtil<MemberInfo> util = new ExcelUtil<MemberInfo>(MemberInfo.class);
        util.exportExcel(response, list, "会员信息数据");
    }

    /**
     * 获取会员信息详细信息
     */
    @GetMapping(value = "/{memberInfoId}")
    public AjaxResult getInfo(@PathVariable("memberInfoId") Long memberInfoId) {
        return success(memberInfoService.selectMemberInfoByMemberInfoId(memberInfoId));
    }

    /**
     * 根据卡号查询会员详细信息
     *
     * @param memberInfoNumber 会员卡号
     * @return 会员详细信息
     */
    @GetMapping(value = "/memberInfoNumber/{memberInfoNumber}")
    public AjaxResult getInfoByMemberInfoNumber(@PathVariable("memberInfoNumber") String memberInfoNumber) {
        return success(memberInfoService.selectMemberInfoByMemberInfoNumber(memberInfoNumber));
    }

    /**
     * 查询卡号是否存在
     *
     * @param memberInfoNumber 会员卡号
     * @return 会员信息ID
     */
    @GetMapping(value = "/memberExistFlag/{memberInfoNumber}")
    public AjaxResult getMemberIdByMemberInfoNumber(@PathVariable("memberInfoNumber") String memberInfoNumber) {
        return success(memberInfoService.selectMemberIdByMemberInfoNumber(memberInfoNumber));
    }

    /**
     * 新增会员信息
     */
    @Log(title = "会员信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MemberInfo memberInfo) {
        return toAjax(memberInfoService.insertMemberInfo(memberInfo));
    }

    /**
     * 修改会员信息
     */
    @Log(title = "会员信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MemberInfo memberInfo) {
        return toAjax(memberInfoService.updateMemberInfo(memberInfo));
    }

    /**
     * 删除会员信息
     */
    @Log(title = "会员信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{memberInfoIds}")
    public AjaxResult remove(@PathVariable Long[] memberInfoIds) {
        return toAjax(memberInfoService.deleteMemberInfoByMemberInfoIds(memberInfoIds));
    }
}
