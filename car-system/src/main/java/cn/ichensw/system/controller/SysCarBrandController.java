package cn.ichensw.system.controller;

import cn.ichensw.common.annotation.Log;
import cn.ichensw.common.core.controller.BaseController;
import cn.ichensw.common.core.domain.AjaxResult;
import cn.ichensw.common.core.page.TableDataInfo;
import cn.ichensw.common.enums.BusinessType;
import cn.ichensw.common.utils.poi.ExcelUtil;
import cn.ichensw.system.domain.SysCarBrand;
import cn.ichensw.system.service.ISysCarBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 汽车品牌Controller
 *
 * @author jucce
 * @date 2022-11-26
 */
@RestController
@RequestMapping("/system/brand")
public class SysCarBrandController extends BaseController {
    @Autowired
    private ISysCarBrandService sysCarBrandService;

    /**
     * 查询汽车品牌列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysCarBrand sysCarBrand) {
        startPage();
        List<SysCarBrand> list = sysCarBrandService.selectSysCarBrandList(sysCarBrand);
        return getDataTable(list);
    }

    /**
     * 导出汽车品牌列表
     */
    @Log(title = "汽车品牌" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysCarBrand sysCarBrand) {
        List<SysCarBrand> list = sysCarBrandService.selectSysCarBrandList(sysCarBrand);
        ExcelUtil<SysCarBrand> util = new ExcelUtil<SysCarBrand>(SysCarBrand.class);
        util.exportExcel(response, list, "汽车品牌数据");
    }

    /**
     * 获取汽车品牌详细信息
     */
    @GetMapping(value = "/{brandId}")
    public AjaxResult getInfo(@PathVariable("brandId") Long brandId) {
        return success(sysCarBrandService.selectSysCarBrandByBrandId(brandId));
    }

    /**
     * 新增汽车品牌
     */
    @Log(title = "汽车品牌" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysCarBrand sysCarBrand) {
        return toAjax(sysCarBrandService.insertSysCarBrand(sysCarBrand));
    }

    /**
     * 修改汽车品牌
     */
    @Log(title = "汽车品牌" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysCarBrand sysCarBrand) {
        return toAjax(sysCarBrandService.updateSysCarBrand(sysCarBrand));
    }

    /**
     * 删除汽车品牌
     */
    @Log(title = "汽车品牌" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{brandIds}")
    public AjaxResult remove(@PathVariable Long[] brandIds) {
        return toAjax(sysCarBrandService.deleteSysCarBrandByBrandIds(brandIds));
    }
}
