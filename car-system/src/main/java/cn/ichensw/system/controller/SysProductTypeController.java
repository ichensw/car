package cn.ichensw.system.controller;

import cn.ichensw.common.annotation.Log;
import cn.ichensw.common.core.controller.BaseController;
import cn.ichensw.common.core.domain.AjaxResult;
import cn.ichensw.common.core.page.TableDataInfo;
import cn.ichensw.common.enums.BusinessType;
import cn.ichensw.common.utils.poi.ExcelUtil;
import cn.ichensw.system.domain.SysProductType;
import cn.ichensw.system.service.ISysProductTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 产品类别Controller
 *
 * @author jucce
 * @date 2022-11-25
 */
@RestController
@RequestMapping("/system/type")
public class SysProductTypeController extends BaseController {
    @Autowired
    private ISysProductTypeService sysProductTypeService;

    /**
     * 查询产品类别列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysProductType sysProductType) {
        List<SysProductType> list = sysProductTypeService.selectSysProductTypeList(sysProductType);
        return getDataTable(list);
    }

    /**
     * 查询产品类别列表
     */
    @GetMapping("/list/page")
    public TableDataInfo listByPage(SysProductType sysProductType) {
        startPage();
        List<SysProductType> list = sysProductTypeService.selectSysProductTypeList(sysProductType);
        return getDataTable(list);
    }

    /**
     * 导出产品类别列表
     */
    @Log(title = "产品类别" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysProductType sysProductType) {
        List<SysProductType> list = sysProductTypeService.selectSysProductTypeList(sysProductType);
        ExcelUtil<SysProductType> util = new ExcelUtil<SysProductType>(SysProductType.class);
        util.exportExcel(response, list, "产品类别数据");
    }

    /**
     * 获取产品类别详细信息
     */
    @GetMapping(value = "/{typeId}")
    public AjaxResult getInfo(@PathVariable("typeId") Long typeId) {
        return success(sysProductTypeService.selectSysProductTypeByTypeId(typeId));
    }


    /**
     * 新增产品类别
     */
    @Log(title = "产品类别" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysProductType sysProductType) {
        return toAjax(sysProductTypeService.insertSysProductType(sysProductType));
    }

    /**
     * 修改产品类别
     */
    @Log(title = "产品类别" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysProductType sysProductType) {
        return toAjax(sysProductTypeService.updateSysProductType(sysProductType));
    }

    /**
     * 删除产品类别
     */
    @Log(title = "产品类别" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{typeIds}")
    public AjaxResult remove(@PathVariable Long[] typeIds) {
        return toAjax(sysProductTypeService.deleteSysProductTypeByTypeIds(typeIds));
    }
}
