package cn.ichensw.system.controller;

import cn.ichensw.common.annotation.Log;
import cn.ichensw.common.core.controller.BaseController;
import cn.ichensw.common.core.domain.AjaxResult;
import cn.ichensw.common.core.page.TableDataInfo;
import cn.ichensw.common.enums.BusinessType;
import cn.ichensw.common.utils.poi.ExcelUtil;
import cn.ichensw.system.domain.SysCarSeries;
import cn.ichensw.system.domain.vo.SysCarSeriesVo;
import cn.ichensw.system.service.ISysCarSeriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 汽车系列Controller
 *
 * @author jucce
 * @date 2022-11-26
 */
@RestController
@RequestMapping("/system/series")
public class SysCarSeriesController extends BaseController {
    @Autowired
    private ISysCarSeriesService sysCarSeriesService;

    /**
     * 查询汽车系列列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysCarSeries sysCarSeries) {
        startPage();
        List<SysCarSeriesVo> list = sysCarSeriesService.selectSysCarSeriesList(sysCarSeries);
        return getDataTable(list);
    }

    /**
     * 导出汽车系列列表
     */
    @Log(title = "汽车系列" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysCarSeriesVo sysCarSeriesVo) {
        List<SysCarSeriesVo> list = sysCarSeriesService.selectSysCarSeriesList(sysCarSeriesVo);
        ExcelUtil<SysCarSeriesVo> util = new ExcelUtil<SysCarSeriesVo>(SysCarSeriesVo.class);
        util.exportExcel(response, list, "汽车系列数据");
    }

    /**
     * 获取汽车系列详细信息
     */
    @GetMapping(value = "/{seriesId}")
    public AjaxResult getInfo(@PathVariable("seriesId") Long seriesId) {
        return success(sysCarSeriesService.selectSysCarSeriesBySeriesId(seriesId));
    }

    /**
     * 新增汽车系列
     */
    @Log(title = "汽车系列" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysCarSeries sysCarSeries) {
        return toAjax(sysCarSeriesService.insertSysCarSeries(sysCarSeries));
    }

    /**
     * 修改汽车系列
     */
    @Log(title = "汽车系列" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysCarSeries sysCarSeries) {
        return toAjax(sysCarSeriesService.updateSysCarSeries(sysCarSeries));
    }

    /**
     * 删除汽车系列
     */
    @Log(title = "汽车系列" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{seriesIds}")
    public AjaxResult remove(@PathVariable Long[] seriesIds) {
        return toAjax(sysCarSeriesService.deleteSysCarSeriesBySeriesIds(seriesIds));
    }
}
