package cn.ichensw.system.controller;

import cn.ichensw.common.annotation.Log;
import cn.ichensw.common.core.controller.BaseController;
import cn.ichensw.common.core.domain.AjaxResult;
import cn.ichensw.common.core.page.TableDataInfo;
import cn.ichensw.common.enums.BusinessType;
import cn.ichensw.common.utils.StringUtils;
import cn.ichensw.common.utils.poi.ExcelUtil;
import cn.ichensw.system.domain.OrderMember;
import cn.ichensw.system.domain.vo.OrderMemberVo;
import cn.ichensw.system.domain.vo.OrderStatisticsVo;
import cn.ichensw.system.service.IOrderMemberService;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.JSONReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 会员消费Controller
 *
 * @author jucce
 * @date 2022-11-25
 */
@RestController
@RequestMapping("/order/member")
public class OrderMemberController extends BaseController {
    @Autowired
    private IOrderMemberService orderMemberService;

    /**
     * 查询会员消费列表
     */
    @GetMapping("/list")
    public TableDataInfo list(OrderMemberVo orderMemberVo) {
        startPage();
        List<OrderMember> list = orderMemberService.selectOrderMemberList(orderMemberVo);
        return getDataTable(list);
    }

    /**
     * 查询订单总数
     */
    @GetMapping("/getOrderCount")
    public AjaxResult getOrderCount() {
        startPage();
        Long orderCount = orderMemberService.selectOrderCount();
        return success().put("data", orderCount);
    }


    /**
     * 查询每个月的订单总数
     */
    @GetMapping("/getMonthOrderCount")
    public AjaxResult getMonthOrderCount() {
        List<OrderStatisticsVo> orderStatisticsVos = orderMemberService.selectMonthOrderCount();
        return success().put("data", orderStatisticsVos);
    }



    /**
     * 查询未结算的会员消费订单列表
     *
     * @return 结果
     */
    @GetMapping("/listAndNoSettlement")
    public TableDataInfo listAndNoSettlement() {
        startPage();
        List<OrderMemberVo> list = orderMemberService.selectOrderMemberListAndNoSettlement();
        return getDataTable(list);
    }

    /**
     * 导出会员消费列表
     */
    @Log(title = "会员消费", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OrderMemberVo orderMemberVo) {
        List<OrderMember> list = orderMemberService.selectOrderMemberList(orderMemberVo);
        ExcelUtil<OrderMember> util = new ExcelUtil<OrderMember>(OrderMember.class);
        util.exportExcel(response, list, "会员消费数据");
    }

    /**
     * 获取会员消费详细信息
     */
    @GetMapping(value = "/{orderMemberId}")
    public AjaxResult getInfo(@PathVariable("orderMemberId") Long orderMemberId) {
        return success(orderMemberService.selectOrderMemberByOrderMemberId(orderMemberId));
    }

    /**
     * 新增会员消费
     */
    @Log(title = "会员消费", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OrderMember orderMember) {
        return toAjax(orderMemberService.insertOrderMember(orderMember));
    }

    /**
     * 修改会员消费
     */
    @Log(title = "会员消费", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OrderMember orderMember) {
        return toAjax(orderMemberService.updateOrderMember(orderMember));
    }

    /**
     * 结算订单
     */
    @Log(title = "会员结算订单", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/checkOutOrderMemberList")
    public AjaxResult checkOutOrderMemberList(@RequestBody Map<String, List<OrderMemberVo>> params) {
        List<OrderMemberVo> list = params.get("list");
        return AjaxResult.success(orderMemberService.updateOrderMemberByOrderMemberId(list));
    }

    /**
     * 删除会员消费
     */
    @Log(title = "会员消费", businessType = BusinessType.DELETE)
    @PostMapping(value = "/deleteMember")
    public AjaxResult remove(@RequestBody Map<String, List<OrderMemberVo>> params) {
        List<OrderMemberVo> list = params.get("list");
        return toAjax(orderMemberService.deleteOrderMemberByOrderMemberIds(list));
    }
}
