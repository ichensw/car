package cn.ichensw.system.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import cn.ichensw.common.config.RuoYiConfig;
import cn.ichensw.common.utils.file.FileUploadUtils;
import cn.ichensw.common.utils.file.FileUtils;
import cn.ichensw.common.utils.file.MimeTypeUtils;
import cn.ichensw.system.domain.vo.StockCarVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.ichensw.common.annotation.Log;
import cn.ichensw.common.core.controller.BaseController;
import cn.ichensw.common.core.domain.AjaxResult;
import cn.ichensw.common.enums.BusinessType;
import cn.ichensw.system.domain.StockCar;
import cn.ichensw.system.service.IStockCarService;
import cn.ichensw.common.utils.poi.ExcelUtil;
import cn.ichensw.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 车辆信息Controller
 * 
 * @author jucce
 * @date 2023-03-05
 */
@RestController
@RequestMapping("/stock/car")
public class StockCarController extends BaseController
{
    @Autowired
    private IStockCarService stockCarService;

    /**
     * 上传车辆信息图片
     *
     * @param file 上传文件
     * @return 图片存储地址
     * @throws IOException 文件读写IO异常
     */
    @Log(title = "预览缩略图", businessType = BusinessType.UPDATE)
    @PostMapping("/picturesUpload")
    public AjaxResult picturesUpload(MultipartFile file) throws Exception {
        if (!file.isEmpty()) {
            // 上传文件，返回上传成功的文件名
            String carImg = FileUploadUtils.upload(RuoYiConfig.getProductImgPath(), file, MimeTypeUtils.IMAGE_EXTENSION);
            if (!StringUtils.isEmpty(carImg)) {
                AjaxResult ajax = AjaxResult.success();
                ajax.put("carImg", carImg);
                return ajax;
            }
        }
        return AjaxResult.error("上传图片异常，请联系管理员");
    }

    /**
     * 删除上传的产品信息图片
     *
     * @param param 图片路径
     * @return 图片查看地址
     */
    @Log(title = "删除上传的图片", businessType = BusinessType.DELETE)
    @PostMapping("/delCarImg")
    public AjaxResult delCarImg(@RequestBody Map<String, String> param) {
        // 修改文件路径
        String filePath = param.get("filePath").replace("/profile/", "/");
        // 删除文件
        boolean flag = FileUtils.deleteFile(RuoYiConfig.getProfile() + filePath);
        System.out.println("删除图片路径：" + RuoYiConfig.getProfile() + filePath + "，删除图片状态：" + flag);
        if (flag) {
            return AjaxResult.success(true);
        }
        return AjaxResult.error("删除图片异常，请联系管理员");
    }


    /**
     * 删除上传的产品信息图片
     *
     * @param param 图片路径
     * @return 图片查看地址
     */
    @Log(title = "批量删除上传的图片", businessType = BusinessType.DELETE)
    @PostMapping("/delMultiProductImg")
    public AjaxResult delMultiProductImg(@RequestBody Map<String, String[]> param) {
        String[] filePaths = param.get("filePaths");
        boolean flag = false;
        for (String filePath : filePaths) {
            // 删除文件
            flag = FileUtils.deleteFile(RuoYiConfig.getProfile() + filePath);
        }
        if (flag) {
            return AjaxResult.success(true);
        }
        return AjaxResult.error("删除图片异常，请联系管理员");
    }

    /**
     * 查询车辆信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(StockCar stockCar)
    {
        startPage();
        List<StockCarVo> list = stockCarService.selectStockCarList(stockCar);
        return getDataTable(list);
    }

    /**
     * 导出车辆信息列表
     */
    @Log(title = "车辆信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockCar stockCar)
    {
        List<StockCarVo> list = stockCarService.selectStockCarList(stockCar);
        ExcelUtil<StockCarVo> util = new ExcelUtil<>(StockCarVo.class);
        util.exportExcel(response, list, "车辆信息数据");
    }

    /**
     * 获取车辆信息详细信息
     */
    @GetMapping(value = "/{carId}")
    public AjaxResult getInfo(@PathVariable("carId") Long carId)
    {
        return AjaxResult.success(stockCarService.selectStockCarByCarId(carId));
    }

    /**
     * 新增车辆信息
     */
    @Log(title = "车辆信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockCar stockCar)
    {
        return toAjax(stockCarService.insertStockCar(stockCar));
    }

    /**
     * 修改车辆信息
     */
    @Log(title = "车辆信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockCar stockCar)
    {
        return toAjax(stockCarService.updateStockCar(stockCar));
    }

    /**
     * 删除车辆信息
     */
    @Log(title = "车辆信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{carIds}")
    public AjaxResult remove(@PathVariable Long[] carIds)
    {
        return toAjax(stockCarService.deleteStockCarByCarIds(carIds));
    }
}
