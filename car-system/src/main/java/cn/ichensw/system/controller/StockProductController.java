package cn.ichensw.system.controller;

import cn.ichensw.common.annotation.Log;
import cn.ichensw.common.config.RuoYiConfig;
import cn.ichensw.common.core.controller.BaseController;
import cn.ichensw.common.core.domain.AjaxResult;
import cn.ichensw.common.core.page.TableDataInfo;
import cn.ichensw.common.enums.BusinessType;
import cn.ichensw.common.utils.file.FileUploadUtils;
import cn.ichensw.common.utils.file.FileUtils;
import cn.ichensw.common.utils.file.MimeTypeUtils;
import cn.ichensw.common.utils.poi.ExcelUtil;
import cn.ichensw.system.domain.StockProduct;
import cn.ichensw.system.domain.vo.StockProductVo;
import cn.ichensw.system.service.IStockProductService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 产品信息Controller
 *
 * @author jucce
 * @date 2022-11-25
 */
@RestController
@RequestMapping("/stock/product")
public class StockProductController extends BaseController {
    @Autowired
    private IStockProductService stockProductService;

    /**
     * 查询产品信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(StockProduct stockProduct) {
        startPage();
        List<StockProductVo> list = stockProductService.selectStockProductList(stockProduct);
        return getDataTable(list);
    }

    /**
     * 导出产品信息列表
     */
    @Log(title = "产品信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockProduct stockProduct) {
        List<StockProductVo> list = stockProductService.selectStockProductList(stockProduct);
        ExcelUtil<StockProductVo> util = new ExcelUtil<StockProductVo>(StockProductVo.class);
        util.exportExcel(response, list, "产品信息数据");
    }

    /**
     * 获取产品信息详细信息
     */
    @GetMapping(value = "/{productId}")
    public AjaxResult getInfo(@PathVariable("productId") Long productId) {
        return success(stockProductService.selectStockProductByProductId(productId));
    }


    /**
     * 根据产品类别查询产品信息
     *
     * @param typeId 产品类别ID
     * @return 产品信息
     */
    @GetMapping(value = "/typeId/{typeId}")
    public AjaxResult getInfoByTypeId(@PathVariable("typeId") Long typeId) {
        return success(stockProductService.selectStockProductByTypeId(typeId));
    }

    /**
     * 新增产品信息
     */
    @Log(title = "产品信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockProduct stockProduct) {
        return toAjax(stockProductService.insertStockProduct(stockProduct));
    }

    /**
     * 修改产品信息
     */
    @Log(title = "产品信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockProduct stockProduct) {
        return toAjax(stockProductService.updateStockProduct(stockProduct));
    }

    /**
     * 删除产品信息
     */
    @Log(title = "产品信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{productIds}")
    public AjaxResult remove(@PathVariable Long[] productIds) {
        return toAjax(stockProductService.deleteStockProductByProductIds(productIds));
    }

    /**
     * 上传产品信息图片
     *
     * @param file 上传文件
     * @return 图片存储地址
     * @throws IOException 文件读写IO异常
     */
    @Log(title = "预览缩略图", businessType = BusinessType.UPDATE)
    @PostMapping("/picturesUpload")
    public AjaxResult picturesUpload(MultipartFile file) throws Exception {

        if (!file.isEmpty()) {
            // 上传文件，返回上传成功的文件名
            String productImg = FileUploadUtils.upload(RuoYiConfig.getProductImgPath(), file, MimeTypeUtils.IMAGE_EXTENSION);
            if (!StringUtils.isEmpty(productImg)) {
                AjaxResult ajax = AjaxResult.success();
                ajax.put("productImg", productImg);
                return ajax;
            }
        }
        return AjaxResult.error("上传图片异常，请联系管理员");
    }

    /**
     * 删除上传的产品信息图片
     *
     * @param param 图片路径
     * @return 图片查看地址
     */
    @Log(title = "删除上传的图片", businessType = BusinessType.DELETE)
    @PostMapping("/delProductImg")
    public AjaxResult delProductImg(@RequestBody Map<String, String> param) {
        // 修改文件路径
        String filePath = param.get("filePath").replace("/profile/", "/");
        // 删除文件
        boolean flag = FileUtils.deleteFile(RuoYiConfig.getProfile() + filePath);
        System.out.println("删除图片路径：" + RuoYiConfig.getProfile() + filePath + "，删除图片状态：" + flag);
        if (flag) {
            return AjaxResult.success(true);
        }
        return AjaxResult.error("删除图片异常，请联系管理员");
    }


    /**
     * 删除上传的产品信息图片
     *
     * @param param 图片路径
     * @return 图片查看地址
     */
    @Log(title = "批量删除上传的图片", businessType = BusinessType.DELETE)
    @PostMapping("/delMultiProductImg")
    public AjaxResult delMultiProductImg(@RequestBody Map<String, String[]> param) {
        String[] filePaths = param.get("filePaths");
        boolean flag = false;
        for (String filePath : filePaths) {
            // 删除文件
            flag = FileUtils.deleteFile(RuoYiConfig.getProfile() + filePath);
        }
        if (flag) {
            return AjaxResult.success(true);
        }
        return AjaxResult.error("删除图片异常，请联系管理员");
    }
}
