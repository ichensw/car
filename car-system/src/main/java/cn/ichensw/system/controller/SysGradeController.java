package cn.ichensw.system.controller;

import cn.ichensw.common.annotation.Log;
import cn.ichensw.common.core.controller.BaseController;
import cn.ichensw.common.core.domain.AjaxResult;
import cn.ichensw.common.core.page.TableDataInfo;
import cn.ichensw.common.enums.BusinessType;
import cn.ichensw.common.utils.poi.ExcelUtil;
import cn.ichensw.system.domain.SysGrade;
import cn.ichensw.system.domain.vo.SysGradeVo;
import cn.ichensw.system.service.ISysGradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 会员等级Controller
 *
 * @author jucce
 * @date 2022-11-25
 */
@RestController
@RequestMapping("/system/grade")
public class SysGradeController extends BaseController {
    @Autowired
    private ISysGradeService sysGradeService;

    /**
     * 查询会员等级列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysGrade sysGrade) {
        startPage();
        List<SysGrade> list = sysGradeService.selectSysGradeList(sysGrade);
        return getDataTable(list);
    }

    /**
     * 查询会员等级 和 各等级对应人数
     */
    @GetMapping("/getMemberNumber")
    public AjaxResult getMemberNumber() {
        List<SysGradeVo> list = sysGradeService.selectSysGradeAndMemberNumber();
        return AjaxResult.success().put("data", list);
    }

    /**
     * 导出会员等级列表
     */
    @Log(title = "会员等级" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysGrade sysGrade) {
        List<SysGrade> list = sysGradeService.selectSysGradeList(sysGrade);
        ExcelUtil<SysGrade> util = new ExcelUtil<SysGrade>(SysGrade.class);
        util.exportExcel(response, list, "会员等级数据");
    }

    /**
     * 获取会员等级详细信息
     */
    @GetMapping(value = "/{gradeId}")
    public AjaxResult getInfo(@PathVariable("gradeId") Long gradeId) {
        return success(sysGradeService.selectSysGradeByGradeId(gradeId));
    }

    /**
     * 新增会员等级
     */
    @Log(title = "会员等级" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysGrade sysGrade) {
        return toAjax(sysGradeService.insertSysGrade(sysGrade));
    }

    /**
     * 修改会员等级
     */
    @Log(title = "会员等级" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysGrade sysGrade) {
        return toAjax(sysGradeService.updateSysGrade(sysGrade));
    }

    /**
     * 删除会员等级
     */
    @Log(title = "会员等级" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{gradeIds}")
    public AjaxResult remove(@PathVariable Long[] gradeIds) {
        return toAjax(sysGradeService.deleteSysGradeByGradeIds(gradeIds));
    }
}
