package cn.ichensw.system.controller;

import cn.ichensw.common.annotation.Log;
import cn.ichensw.common.constant.HttpStatus;
import cn.ichensw.common.core.controller.BaseController;
import cn.ichensw.common.core.domain.AjaxResult;
import cn.ichensw.common.core.page.TableDataInfo;
import cn.ichensw.common.enums.BusinessType;
import cn.ichensw.common.utils.poi.ExcelUtil;
import cn.ichensw.system.domain.MemberInfo;
import cn.ichensw.system.domain.MemberRecharge;
import cn.ichensw.system.domain.vo.MemberRechargeVo;
import cn.ichensw.system.service.IMemberRechargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 会员充值Controller
 *
 * @author jucce
 * @date 2022-11-25
 */
@RestController
@RequestMapping("/member/recharge")
public class MemberRechargeController extends BaseController {
    @Autowired
    private IMemberRechargeService memberRechargeService;

    /**
     * 查询会员充值列表
     */
    @GetMapping("/list")
    public TableDataInfo list(MemberRechargeVo memberRecharge) {
        startPage();
        List<MemberRechargeVo> list = memberRechargeService.selectMemberRechargeList(memberRecharge);

        return getDataTable(list);
    }

    /**
     * 导出会员充值列表
     */
    @Log(title = "会员充值", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MemberRechargeVo memberRechargeVo) {
        List<MemberRechargeVo> list = memberRechargeService.selectMemberRechargeList(memberRechargeVo);
        ExcelUtil<MemberRechargeVo> util = new ExcelUtil<MemberRechargeVo>(MemberRechargeVo.class);
        util.exportExcel(response, list, "会员充值数据");
    }

    /**
     * 获取会员充值详细信息
     */
    @GetMapping(value = "/{rechargeId}")
    public AjaxResult getInfo(@PathVariable("rechargeId") Long rechargeId) {
        return success(memberRechargeService.selectMemberRechargeByRechargeId(rechargeId));
    }

    /**
     * 新增会员充值
     */
    @Log(title = "会员充值", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MemberRecharge memberRecharge) {
        return toAjax(memberRechargeService.insertMemberRecharge(memberRecharge));
    }

    /**
     * 修改会员充值
     */
    @Log(title = "会员充值", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MemberRecharge memberRecharge) {
        return toAjax(memberRechargeService.updateMemberRecharge(memberRecharge));
    }

    /**
     * 根据充值金额获取匹配的优惠活动信息
     */
    @PostMapping(value = "/checkPassword")
    public AjaxResult checkNumberAndPassword(@RequestBody MemberInfo memberInfo) {
        return success(memberRechargeService.checkNumberAndPassword(memberInfo));
    }

    /**
     * 删除会员充值
     */
    @Log(title = "会员充值", businessType = BusinessType.DELETE)
    @DeleteMapping("/{rechargeIds}")
    public AjaxResult remove(@PathVariable Long[] rechargeIds) {
        return toAjax(memberRechargeService.deleteMemberRechargeByRechargeIds(rechargeIds));
    }
}
