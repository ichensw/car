package cn.ichensw.system.domain.vo;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.page.PageDomain;

import java.math.BigDecimal;

public class MemberRechargeVo extends PageDomain {

    /**
     * 会员充值ID
     */
    private Long rechargeId;

    /**
     * 会员卡号
     */
    @Excel(name = "会员卡号")
    private String memberInfoNumber;

    /**
     * 充值金额
     */
    @Excel(name = "充值金额")
    private BigDecimal rechargeAmount;

    /**
     * 优惠活动名称
     */
    @Excel(name = "优惠活动名称")
    private String activiesName;
    /**
     * 充值优惠金额
     */
    @Excel(name = "赠送金额")
    private BigDecimal rechargeGiveAmount;

    /**
     * 充值备注
     */
    @Excel(name = "充值备注")
    private String rechargeRemark;

    /**
     * 会员信息ID
     */
    private Long memberInfoId;

    /**
     * 管理员ID
     */
    private Long userId;

    /**
     * 优惠活动ID
     */
    private Long activiesId;

    /**
     * 用户名
     */
    @Excel(name = "用户名")
    private String userName;

    public Long getRechargeId() {
        return rechargeId;
    }

    public void setRechargeId(Long rechargeId) {
        this.rechargeId = rechargeId;
    }

    public BigDecimal getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(BigDecimal rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public BigDecimal getRechargeGiveAmount() {
        return rechargeGiveAmount;
    }

    public void setRechargeGiveAmount(BigDecimal rechargeGiveAmount) {
        this.rechargeGiveAmount = rechargeGiveAmount;
    }

    public String getRechargeRemark() {
        return rechargeRemark;
    }

    public void setRechargeRemark(String rechargeRemark) {
        this.rechargeRemark = rechargeRemark;
    }

    public Long getMemberInfoId() {
        return memberInfoId;
    }

    public void setMemberInfoId(Long memberInfoId) {
        this.memberInfoId = memberInfoId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getActiviesId() {
        return activiesId;
    }

    public void setActiviesId(Long activiesId) {
        this.activiesId = activiesId;
    }

    public String getMemberInfoNumber() {
        return memberInfoNumber;
    }

    public void setMemberInfoNumber(String memberInfoNumber) {
        this.memberInfoNumber = memberInfoNumber;
    }

    public String getActiviesName() {
        return activiesName;
    }

    public void setActiviesName(String activiesName) {
        this.activiesName = activiesName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
