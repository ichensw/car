package cn.ichensw.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;

/**
 * 车辆信息对象 stock_car
 * 
 * @author jucce
 * @date 2023-03-05
 */
public class StockCar extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 车辆ID */
    private Long carId;

    /** 车辆系列 */
    @Excel(name = "车辆系列")
    private Long seriesId;

    /** 车辆品牌 */
    @Excel(name = "车辆品牌")
    private Long brandId;

    /** 汽车名称 */
    @Excel(name = "汽车名称")
    private String carName;

    /** 座位数 */
    @Excel(name = "座位数")
    private Long carSeatsNum;

    /** 换挡方式 */
    @Excel(name = "换挡方式")
    private String carShift;

    /** 数量 */
    @Excel(name = "数量")
    private Long carNum;

    /** 售价 */
    @Excel(name = "售价")
    private BigDecimal carPrice;

    /** 车辆图片 */
    @Excel(name = "车辆图片")
    private String carImg;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Date createTime;

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public void setCarId(Long carId)
    {
        this.carId = carId;
    }

    public Long getCarId()
    {
        return carId;
    }
    public void setSeriesId(Long seriesId)
    {
        this.seriesId = seriesId;
    }

    public Long getSeriesId()
    {
        return seriesId;
    }
    public void setCarSeatsNum(Long carSeatsNum)
    {
        this.carSeatsNum = carSeatsNum;
    }

    public Long getCarSeatsNum()
    {
        return carSeatsNum;
    }
    public void setCarShift(String carShift)
    {
        this.carShift = carShift;
    }

    public String getCarShift()
    {
        return carShift;
    }
    public void setCarNum(Long carNum)
    {
        this.carNum = carNum;
    }

    public Long getCarNum()
    {
        return carNum;
    }
    public void setCarPrice(BigDecimal carPrice)
    {
        this.carPrice = carPrice;
    }

    public BigDecimal getCarPrice()
    {
        return carPrice;
    }
    public void setCarImg(String carImg)
    {
        this.carImg = carImg;
    }

    public String getCarImg()
    {
        return carImg;
    }

    @Override
    public String toString() {
        return "StockCar{" +
                "carId=" + carId +
                ", seriesId=" + seriesId +
                ", brandId=" + brandId +
                ", carName='" + carName + '\'' +
                ", carSeatsNum=" + carSeatsNum +
                ", carShift='" + carShift + '\'' +
                ", carNum=" + carNum +
                ", carPrice=" + carPrice +
                ", carImg='" + carImg + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
