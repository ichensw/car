package cn.ichensw.system.domain.vo;

import cn.ichensw.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

public class OrderSpreadVo {

    /**
     * 散客消费ID
     */
    private Long spreadId;

    /**
     * 购买数量
     */
    @Excel(name = "购买数量")
    private Long spreadCount;

    /**
     * 购买姓名
     */
    @Excel(name = "购买姓名")
    private String spreadName;

    /**
     * 购买电话
     */
    @Excel(name = "购买电话")
    private String spreadPhone;

    /**
     * 购买时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "购买时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date spreadTime;

    /**
     * 产品ID
     */
    @Excel(name = "产品ID")
    private Long productId;

    /**
     * 产品名称
     */
    @Excel(name = "产品名称")
    private String productName;

    /**
     * 产品售价
     */
    @Excel(name = "产品售价")
    private BigDecimal productOutAmount;

    /**
     * 产品规格
     */
    @Excel(name = "产品规格")
    private String productSpecifications;

    /**
     * 管理员ID
     */
    @Excel(name = "管理员ID")
    private Long userId;

    /**
     * 用户账号
     */
    @Excel(name = "登录名称")
    private String userName;


    /**
     * 产品类别名称
     */
    @Excel(name = "产品类别名称")
    private String typeName;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getProductOutAmount() {
        return productOutAmount;
    }

    public void setProductOutAmount(BigDecimal productOutAmount) {
        this.productOutAmount = productOutAmount;
    }

    public String getProductSpecifications() {
        return productSpecifications;
    }

    public void setProductSpecifications(String productSpecifications) {
        this.productSpecifications = productSpecifications;
    }

    public void setSpreadId(Long spreadId) {
        this.spreadId = spreadId;
    }

    public Long getSpreadId() {
        return spreadId;
    }

    public void setSpreadCount(Long spreadCount) {
        this.spreadCount = spreadCount;
    }

    public Long getSpreadCount() {
        return spreadCount;
    }

    public void setSpreadName(String spreadName) {
        this.spreadName = spreadName;
    }

    public String getSpreadName() {
        return spreadName;
    }

    public void setSpreadPhone(String spreadPhone) {
        this.spreadPhone = spreadPhone;
    }

    public String getSpreadPhone() {
        return spreadPhone;
    }

    public void setSpreadTime(Date spreadTime) {
        this.spreadTime = spreadTime;
    }

    public Date getSpreadTime() {
        return spreadTime;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("spreadId" , getSpreadId())
                .append("spreadCount" , getSpreadCount())
                .append("spreadName" , getSpreadName())
                .append("spreadPhone" , getSpreadPhone())
                .append("spreadTime" , getSpreadTime())
                .append("productId" , getProductId())
                .append("userId" , getUserId())
                .toString();
    }
}
