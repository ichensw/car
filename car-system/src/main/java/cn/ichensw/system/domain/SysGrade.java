package cn.ichensw.system.domain;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 会员等级对象 sys_grade
 *
 * @author jucce
 * @date 2022-11-25
 */
public class SysGrade extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 会员等级ID
     */
    private Long gradeId;

    /**
     * 等级名称
     */
    @Excel(name = "等级名称")
    private String gradeName;

    /**
     * 等级所需积分
     */
    @Excel(name = "等级所需积分")
    private Long gradeIntegral;

    /**
     * 等级消费折扣
     */
    @Excel(name = "等级消费折扣")
    private BigDecimal gradeDiscount;

    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }

    public Long getGradeId() {
        return gradeId;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeIntegral(Long gradeIntegral) {
        this.gradeIntegral = gradeIntegral;
    }

    public Long getGradeIntegral() {
        return gradeIntegral;
    }

    public void setGradeDiscount(BigDecimal gradeDiscount) {
        this.gradeDiscount = gradeDiscount;
    }

    public BigDecimal getGradeDiscount() {
        return gradeDiscount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("gradeId" , getGradeId())
                .append("gradeName" , getGradeName())
                .append("gradeIntegral" , getGradeIntegral())
                .append("gradeDiscount" , getGradeDiscount())
                .toString();
    }
}
