package cn.ichensw.system.domain;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 产品类别对象 sys_product_type
 *
 * @author jucce
 * @date 2022-11-25
 */
public class SysProductType extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 产品类别ID
     */
    private Long typeId;

    /**
     * 产品类别名称
     */
    @Excel(name = "产品类别名称")
    private String typeName;

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("typeId" , getTypeId())
                .append("typeName" , getTypeName())
                .toString();
    }
}
