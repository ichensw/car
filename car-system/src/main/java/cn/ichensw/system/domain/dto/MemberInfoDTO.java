package cn.ichensw.system.domain.dto;

import cn.ichensw.common.annotation.Excel;

public class MemberInfoDTO {

    /**
     * 会员信息ID
     */
    private Long memberInfoId;

    /**
     * 会员积分
     */
    private Long memberInfoIntegral;

    /**
     * 会员等级ID
     */
    private Long gradeId;

    public Long getGradeId() {
        return gradeId;
    }

    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }

    public Long getMemberInfoId() {
        return memberInfoId;
    }

    public void setMemberInfoId(Long memberInfoId) {
        this.memberInfoId = memberInfoId;
    }

    public Long getMemberInfoIntegral() {
        return memberInfoIntegral;
    }

    public void setMemberInfoIntegral(Long memberInfoIntegral) {
        this.memberInfoIntegral = memberInfoIntegral;
    }
}
