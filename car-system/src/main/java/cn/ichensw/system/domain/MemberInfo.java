package cn.ichensw.system.domain;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * 会员信息对象 member_info
 *
 * @author jucce
 * @date 2022-11-25
 */
public class MemberInfo extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 会员信息ID
     */
    private Long memberInfoId;

    /**
     * 会员卡号
     */
    @Excel(name = "会员卡号")
    private String memberInfoNumber;

    /**
     * 会员名称
     */
    @Excel(name = "会员名称")
    private String memberInfoName;

    /**
     * 会员余额
     */
    @Excel(name = "会员余额")
    private BigDecimal memberInfoMoney;

    /**
     * 会员电话
     */
    @Excel(name = "会员电话")
    private String memberInfoPhone;

    /**
     * 会员电话
     */
    @Excel(name = "支付密码")
    private String memberInfoPassword;

    /**
     * 会员性别
     */
    @Excel(name = "会员性别")
    private String memberInfoSex;

    /**
     * 会员性别
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "会员生日" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date memberInfoBirthday;

    /**
     * 会员积分
     */
    @Excel(name = "会员积分")
    private Long memberInfoIntegral;

    /**
     * 车牌号
     */
    @Excel(name = "车牌号")
    private String memberInfoCarNumber;

    /**
     * 汽车颜色
     */
    @Excel(name = "汽车颜色")
    private String memberInfoCarColor;

    /**
     * 行驶里程
     */
    @Excel(name = "行驶里程")
    private BigDecimal memberInfoMileage;

    /**
     * 会员证件编号
     */
    @Excel(name = "会员证件编号")
    private String memberInfoCertNumber;

    /**
     * 会员居住地址
     */
    @Excel(name = "会员居住地址")
    private String memberInfoAddress;

    /**
     * 会员开通时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "会员开通时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date memberInfoCreateTime;

    /**
     * 会员备注
     */
    @Excel(name = "会员备注")
    private String memberInfoRemark;

    /**
     * 会员等级ID
     */
    @Excel(name = "会员等级ID")
    private Long gradeId;

    /**
     * 汽车系列ID
     */
    @Excel(name = "汽车系列ID")
    private Long seriesId;

    /**
     * 证件ID
     */
    @Excel(name = "证件ID")
    private Long certId;

    /**
     * 汽车品牌ID
     */
    @Excel(name = "汽车品牌ID")
    private Long brandId;

    public void setMemberInfoId(Long memberInfoId) {
        this.memberInfoId = memberInfoId;
    }

    public Long getMemberInfoId() {
        return memberInfoId;
    }

    public void setMemberInfoNumber(String memberInfoNumber) {
        this.memberInfoNumber = memberInfoNumber;
    }

    public String getMemberInfoNumber() {
        return memberInfoNumber;
    }

    public void setMemberInfoName(String memberInfoName) {
        this.memberInfoName = memberInfoName;
    }

    public String getMemberInfoName() {
        return memberInfoName;
    }

    public void setMemberInfoMoney(BigDecimal memberInfoMoney) {
        this.memberInfoMoney = memberInfoMoney;
    }

    public BigDecimal getMemberInfoMoney() {
        return memberInfoMoney;
    }

    public void setMemberInfoPhone(String memberInfoPhone) {
        this.memberInfoPhone = memberInfoPhone;
    }

    public String getMemberInfoPhone() {
        return memberInfoPhone;
    }

    public void setMemberInfoSex(String memberInfoSex) {
        this.memberInfoSex = memberInfoSex;
    }

    public String getMemberInfoSex() {
        return memberInfoSex;
    }

    public void setMemberInfoIntegral(Long memberInfoIntegral) {
        this.memberInfoIntegral = memberInfoIntegral;
    }

    public Long getMemberInfoIntegral() {
        return memberInfoIntegral;
    }

    public void setMemberInfoCarNumber(String memberInfoCarNumber) {
        this.memberInfoCarNumber = memberInfoCarNumber;
    }

    public String getMemberInfoCarNumber() {
        return memberInfoCarNumber;
    }

    public void setMemberInfoCarColor(String memberInfoCarColor) {
        this.memberInfoCarColor = memberInfoCarColor;
    }

    public String getMemberInfoCarColor() {
        return memberInfoCarColor;
    }

    public void setMemberInfoMileage(BigDecimal memberInfoMileage) {
        this.memberInfoMileage = memberInfoMileage;
    }

    public BigDecimal getMemberInfoMileage() {
        return memberInfoMileage;
    }

    public void setMemberInfoCertNumber(String memberInfoCertNumber) {
        this.memberInfoCertNumber = memberInfoCertNumber;
    }

    public String getMemberInfoCertNumber() {
        return memberInfoCertNumber;
    }

    public void setMemberInfoAddress(String memberInfoAddress) {
        this.memberInfoAddress = memberInfoAddress;
    }

    public String getMemberInfoAddress() {
        return memberInfoAddress;
    }

    public void setMemberInfoCreateTime(Date memberInfoCreateTime) {
        this.memberInfoCreateTime = memberInfoCreateTime;
    }

    public Date getMemberInfoCreateTime() {
        return memberInfoCreateTime;
    }

    public void setMemberInfoRemark(String memberInfoRemark) {
        this.memberInfoRemark = memberInfoRemark;
    }

    public String getMemberInfoRemark() {
        return memberInfoRemark;
    }

    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }

    public Long getGradeId() {
        return gradeId;
    }

    public void setSeriesId(Long seriesId) {
        this.seriesId = seriesId;
    }

    public Long getSeriesId() {
        return seriesId;
    }

    public void setCertId(Long certId) {
        this.certId = certId;
    }

    public Long getCertId() {
        return certId;
    }

    public String getMemberInfoPassword() {
        return memberInfoPassword;
    }

    public void setMemberInfoPassword(String memberInfoPassword) {
        this.memberInfoPassword = memberInfoPassword;
    }

    public Date getMemberInfoBirthday() {
        return memberInfoBirthday;
    }

    public void setMemberInfoBirthday(Date memberInfoBirthday) {
        this.memberInfoBirthday = memberInfoBirthday;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("memberInfoId" , getMemberInfoId())
                .append("memberInfoNumber" , getMemberInfoNumber())
                .append("memberInfoName" , getMemberInfoName())
                .append("memberInfoMoney" , getMemberInfoMoney())
                .append("memberInfoPhone" , getMemberInfoPhone())
                .append("memberInfoPassword" , getMemberInfoPassword())
                .append("memberInfoSex" , getMemberInfoSex())
                .append("memberInfoBirthday" , getMemberInfoBirthday())
                .append("memberInfoIntegral" , getMemberInfoIntegral())
                .append("memberInfoCarNumber" , getMemberInfoCarNumber())
                .append("memberInfoCarColor" , getMemberInfoCarColor())
                .append("memberInfoMileage" , getMemberInfoMileage())
                .append("memberInfoCertNumber" , getMemberInfoCertNumber())
                .append("memberInfoAddress" , getMemberInfoAddress())
                .append("memberInfoCreateTime" , getMemberInfoCreateTime())
                .append("memberInfoRemark" , getMemberInfoRemark())
                .append("gradeId" , getGradeId())
                .append("seriesId" , getSeriesId())
                .append("certId" , getCertId())
                .append("brandId" , getBrandId())
                .toString();
    }
}
