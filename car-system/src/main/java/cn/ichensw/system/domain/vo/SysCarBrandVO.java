package cn.ichensw.system.domain.vo;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 汽车品牌对象 sys_car_brand
 *
 * @author jucce
 * @date 2023-4-12
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysCarBrandVO extends BaseEntity {

    private static final long serialVersionUID = -1542584084789175326L;

    /**
     * 汽车品牌ID
     */
    private Long brandId;

    /**
     * 汽车品牌名称
     */
    @Excel(name = "汽车品牌名称")
    private String brandName;

    /**
     * 汽车系列ID
     */
    private Long seriesId;

    /**
     * 汽车品牌名称
     */
    @Excel(name = "汽车系列名称")
    private String seriesName;

}
