package cn.ichensw.system.domain;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * 优惠活动对象 sys_activies
 *
 * @author jucce
 * @date 2022-11-25
 */
public class SysActivies extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 优惠活动ID
     */
    private Long activiesId;

    /**
     * 优惠活动名称
     */
    @Excel(name = "优惠活动名称")
    private String activiesName;

    /**
     * 开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "开始时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date activiesBeginTime;

    /**
     * 结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "结束时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date activiesEndTime;

    /**
     * 赠送金额
     */
    @Excel(name = "赠送金额")
    private BigDecimal activiesAmount;

    /**
     * 充值金额
     */
    @Excel(name = "充值金额")
    private BigDecimal activiesRechargeAmount;

    public Long getActiviesId() {
        return activiesId;
    }

    public void setActiviesId(Long activiesId) {
        this.activiesId = activiesId;
    }

    public String getActiviesName() {
        return activiesName;
    }

    public void setActiviesName(String activiesName) {
        this.activiesName = activiesName;
    }

    public Date getActiviesBeginTime() {
        return activiesBeginTime;
    }

    public void setActiviesBeginTime(Date activiesBeginTime) {
        this.activiesBeginTime = activiesBeginTime;
    }

    public Date getActiviesEndTime() {
        return activiesEndTime;
    }

    public void setActiviesEndTime(Date activiesEndTime) {
        this.activiesEndTime = activiesEndTime;
    }

    public BigDecimal getActiviesAmount() {
        return activiesAmount;
    }

    public void setActiviesAmount(BigDecimal activiesAmount) {
        this.activiesAmount = activiesAmount;
    }

    public BigDecimal getActiviesRechargeAmount() {
        return activiesRechargeAmount;
    }

    public void setActiviesRechargeAmount(BigDecimal activiesRechargeAmount) {
        this.activiesRechargeAmount = activiesRechargeAmount;
    }

    @Override
    public String toString() {
        return "SysActivies{" +
                "activiesId=" + activiesId +
                ", activiesName='" + activiesName + '\'' +
                ", activiesBeginTime=" + activiesBeginTime +
                ", activiesEndTime=" + activiesEndTime +
                ", activiesAmount=" + activiesAmount +
                ", activiesRechargeAmount=" + activiesRechargeAmount +
                '}';
    }
}
