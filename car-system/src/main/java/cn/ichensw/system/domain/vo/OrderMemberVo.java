package cn.ichensw.system.domain.vo;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;

import java.math.BigDecimal;

public class OrderMemberVo extends BaseEntity {

    /**
     * 会员消费ID
     */
    private Long orderMemberId;

    /**
     * 购买数量
     */
    @Excel(name = "购买数量")
    private Long orderMemberCount;

    /**
     * 会员订单状态(0:未结账 1:已结账)
     */
    @Excel(name = "会员订单状态(0:未结账 1:已结账)")
    private String orderMemberStatus;

    /**
     * 会员信息ID
     */
    @Excel(name = "会员信息ID")
    private Long memberInfoId;

    /**
     * 管理员ID
     */
    @Excel(name = "管理员ID")
    private Long userId;

    /**
     * 产品ID
     */
    @Excel(name = "产品ID")
    private Long productId;

    /**
     * 产品名称
     */
    @Excel(name = "产品名称")
    private String productName;

    /**
     * 产品类别名称
     */
    @Excel(name = "产品类别名称")
    private String typeName;

    /**
     * 产品售价
     */
    @Excel(name = "产品售价")
    private BigDecimal productOutAmount;

    /**
     * 产品规格
     */
    @Excel(name = "产品规格")
    private String productSpecifications;

    /**
     * 会员卡号
     */
    @Excel(name = "会员卡号")
    private String memberInfoNumber;

    /**
     * 会员名称
     */
    @Excel(name = "会员名称")
    private String memberInfoName;

    /**
     * 用户名
     */
    @Excel(name = "用户名")
    private String userName;

    /**
     * 会员余额
     */
    @Excel(name = "会员余额")
    private BigDecimal memberInfoMoney;

    /**
     * 等级消费折扣
     */
    @Excel(name = "等级消费折扣")
    private BigDecimal gradeDiscount;

    /**
     * 会员积分
     */
    @Excel(name = "会员积分")
    private Long memberInfoIntegral;

    public Long getMemberInfoIntegral() {
        return memberInfoIntegral;
    }

    public void setMemberInfoIntegral(Long memberInfoIntegral) {
        this.memberInfoIntegral = memberInfoIntegral;
    }

    public BigDecimal getGradeDiscount() {
        return gradeDiscount;
    }

    public void setGradeDiscount(BigDecimal gradeDiscount) {
        this.gradeDiscount = gradeDiscount;
    }

    public BigDecimal getMemberInfoMoney() {
        return memberInfoMoney;
    }

    public void setMemberInfoMoney(BigDecimal memberInfoMoney) {
        this.memberInfoMoney = memberInfoMoney;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMemberInfoNumber() {
        return memberInfoNumber;
    }

    public void setMemberInfoNumber(String memberInfoNumber) {
        this.memberInfoNumber = memberInfoNumber;
    }

    public String getMemberInfoName() {
        return memberInfoName;
    }

    public void setMemberInfoName(String memberInfoName) {
        this.memberInfoName = memberInfoName;
    }

    public String getProductSpecifications() {
        return productSpecifications;
    }

    public void setProductSpecifications(String productSpecifications) {
        this.productSpecifications = productSpecifications;
    }

    public Long getOrderMemberId() {
        return orderMemberId;
    }

    public void setOrderMemberId(Long orderMemberId) {
        this.orderMemberId = orderMemberId;
    }

    public Long getOrderMemberCount() {
        return orderMemberCount;
    }

    public void setOrderMemberCount(Long orderMemberCount) {
        this.orderMemberCount = orderMemberCount;
    }

    public String getOrderMemberStatus() {
        return orderMemberStatus;
    }

    public void setOrderMemberStatus(String orderMemberStatus) {
        this.orderMemberStatus = orderMemberStatus;
    }

    public Long getMemberInfoId() {
        return memberInfoId;
    }

    public void setMemberInfoId(Long memberInfoId) {
        this.memberInfoId = memberInfoId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public BigDecimal getProductOutAmount() {
        return productOutAmount;
    }

    public void setProductOutAmount(BigDecimal productOutAmount) {
        this.productOutAmount = productOutAmount;
    }
}
