package cn.ichensw.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


public class OrderStatisticsVo {

    private String month;

    private Long orderCount;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Long getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Long orderCount) {
        this.orderCount = orderCount;
    }
}
