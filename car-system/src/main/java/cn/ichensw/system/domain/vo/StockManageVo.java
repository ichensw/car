package cn.ichensw.system.domain.vo;

import cn.ichensw.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class StockManageVo {

    /**
     * 库存管理ID
     */
    private Long manageId;

    /**
     * 入库数量
     */
    @Excel(name = "入库数量")
    private Long manageCount;

    /**
     * 入库时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "入库时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date manageTime;

    /**
     * 产品ID
     */
    @Excel(name = "产品ID")
    private Long productId;
    /**
     * 产品名称
     */
    @Excel(name = "产品名称")
    private String productName;

    /**
     * 管理员ID
     */
    @Excel(name = "管理员ID")
    private Long userId;
    /**
     * 产品类别ID
     */
    @Excel(name = "产品类别ID")
    private Long typeId;
    /**
     * 产品类别名称
     */
    @Excel(name = "产品类别名称")
    private String typeName;
    /**
     * 用户名
     */
    @Excel(name = "用户名称")
    private String nickName;

    public Long getManageId() {
        return manageId;
    }

    public void setManageId(Long manageId) {
        this.manageId = manageId;
    }

    public Long getManageCount() {
        return manageCount;
    }

    public void setManageCount(Long manageCount) {
        this.manageCount = manageCount;
    }

    public Date getManageTime() {
        return manageTime;
    }

    public void setManageTime(Date manageTime) {
        this.manageTime = manageTime;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Override
    public String toString() {
        return "StockManageDTO{" +
                "manageId=" + manageId +
                ", manageCount=" + manageCount +
                ", manageTime=" + manageTime +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", userId=" + userId +
                ", typeId=" + typeId +
                ", typeName='" + typeName + '\'' +
                ", userName='" + nickName + '\'' +
                '}';
    }
}
