package cn.ichensw.system.domain;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 散客消费对象 order_spread
 *
 * @author jucce
 * @date 2022-11-25
 */
public class OrderSpread extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 散客消费ID
     */
    private Long spreadId;

    /**
     * 购买数量
     */
    @Excel(name = "购买数量")
    private Long spreadCount;

    /**
     * 购买姓名
     */
    @Excel(name = "购买姓名")
    private String spreadName;

    /**
     * 购买电话
     */
    @Excel(name = "购买电话")
    private String spreadPhone;

    /**
     * 购买时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "购买时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date spreadTime;

    /**
     * 产品ID
     */
    @Excel(name = "产品ID")
    private Long productId;

    /**
     * 管理员ID
     */
    @Excel(name = "管理员ID")
    private Long userId;

    public void setSpreadId(Long spreadId) {
        this.spreadId = spreadId;
    }

    public Long getSpreadId() {
        return spreadId;
    }

    public void setSpreadCount(Long spreadCount) {
        this.spreadCount = spreadCount;
    }

    public Long getSpreadCount() {
        return spreadCount;
    }

    public void setSpreadName(String spreadName) {
        this.spreadName = spreadName;
    }

    public String getSpreadName() {
        return spreadName;
    }

    public void setSpreadPhone(String spreadPhone) {
        this.spreadPhone = spreadPhone;
    }

    public String getSpreadPhone() {
        return spreadPhone;
    }

    public void setSpreadTime(Date spreadTime) {
        this.spreadTime = spreadTime;
    }

    public Date getSpreadTime() {
        return spreadTime;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("spreadId" , getSpreadId())
                .append("spreadCount" , getSpreadCount())
                .append("spreadName" , getSpreadName())
                .append("spreadPhone" , getSpreadPhone())
                .append("spreadTime" , getSpreadTime())
                .append("productId" , getProductId())
                .append("userId" , getUserId())
                .toString();
    }
}
