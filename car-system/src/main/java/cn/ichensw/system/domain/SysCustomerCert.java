package cn.ichensw.system.domain;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 客户凭证对象 sys_customer_cert
 *
 * @author jucce
 * @date 2022-11-26
 */
public class SysCustomerCert extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 证件ID
     */
    private Long certId;

    /**
     * 证件类型
     */
    @Excel(name = "证件类型 ")
    private String certName;

    public void setCertId(Long certId) {
        this.certId = certId;
    }

    public Long getCertId() {
        return certId;
    }

    public void setCertName(String certName) {
        this.certName = certName;
    }

    public String getCertName() {
        return certName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("certId" , getCertId())
                .append("certName" , getCertName())
                .toString();
    }
}
