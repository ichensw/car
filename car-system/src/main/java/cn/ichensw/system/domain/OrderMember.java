package cn.ichensw.system.domain;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 会员消费对象 order_member
 *
 * @author jucce
 * @date 2022-11-25
 */
public class OrderMember extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 会员消费ID
     */
    private Long orderMemberId;

    /**
     * 购买数量
     */
    @Excel(name = "购买数量")
    private Long orderMemberCount;

    /**
     * 会员订单状态(0:未结账 1:已结账)
     */
    @Excel(name = "会员订单状态(0:未结账 1:已结账)")
    private String orderMemberStatus;

    /**
     * 会员信息ID
     */
    @Excel(name = "会员信息ID")
    private Long memberInfoId;

    /**
     * 管理员ID
     */
    @Excel(name = "管理员ID")
    private Long userId;

    /**
     * 产品ID
     */
    @Excel(name = "产品ID")
    private Long productId;

    public void setOrderMemberId(Long orderMemberId) {
        this.orderMemberId = orderMemberId;
    }

    public Long getOrderMemberId() {
        return orderMemberId;
    }

    public void setOrderMemberCount(Long orderMemberCount) {
        this.orderMemberCount = orderMemberCount;
    }

    public Long getOrderMemberCount() {
        return orderMemberCount;
    }

    public void setOrderMemberStatus(String orderMemberStatus) {
        this.orderMemberStatus = orderMemberStatus;
    }

    public String getOrderMemberStatus() {
        return orderMemberStatus;
    }

    public void setMemberInfoId(Long memberInfoId) {
        this.memberInfoId = memberInfoId;
    }

    public Long getMemberInfoId() {
        return memberInfoId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProductId() {
        return productId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("orderMemberId" , getOrderMemberId())
                .append("orderMemberCount" , getOrderMemberCount())
                .append("orderMemberStatus" , getOrderMemberStatus())
                .append("createTime" , getCreateTime())
                .append("memberInfoId" , getMemberInfoId())
                .append("userId" , getUserId())
                .append("productId" , getProductId())
                .toString();
    }
}
