package cn.ichensw.system.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 车辆销售对象 order_car
 *
 * @author jucce
 * @date 2023-03-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderCar extends BaseEntity {

    private static final long serialVersionUID = -8985895603170604030L;
    /**
     * 车辆销售ID
     */
    @Excel(name = "车辆销售ID")
    private Long orderCarId;

    /**
     * 车辆ID
     */
    @Excel(name = "车辆ID")
    private Long carId;

    /**
     * 经办人
     */
    @Excel(name = "经办人")
    private Long userId;

    /**
     * 是否支付（0-未支付，1-已支付）
     */
    @Excel(name = "是否支付", readConverterExp = "0=-未支付，1-已支付")
    private Long isPay;

    /**
     * 支付方式（0-全额付款，1-分期付款）
     */
    @Excel(name = "支付方式", readConverterExp = "0=-全额付款，1-分期付款")
    private Long payMethod;

    /**
     * 客户名称
     */
    @Excel(name = "客户名称")
    private String customerName;

    /**
     * 客户电话
     */
    @Excel(name = "客户电话")
    private String customerPhone;

    /**
     * 数量
     */
    @Excel(name = "数量")
    private Integer carNum;

    /**
     * 交付时间
     */
    @Excel(name = "交付时间")
    private Date deliveryTime;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    private Date createTime;
}
