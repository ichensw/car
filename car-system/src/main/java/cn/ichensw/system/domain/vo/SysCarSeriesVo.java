package cn.ichensw.system.domain.vo;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.system.domain.SysCarSeries;

public class SysCarSeriesVo extends SysCarSeries {

    /**
     * 汽车系列ID
     */
    private Long seriesId;

    /**
     * 汽车系列名称
     */
    @Excel(name = "汽车系列名称")
    private String seriesName;

    /**
     * 汽车品牌ID
     */
    private Long brandId;

    @Excel(name = "汽车品牌名称")
    private String brandName;

    @Override
    public Long getSeriesId() {
        return seriesId;
    }

    @Override
    public void setSeriesId(Long seriesId) {
        this.seriesId = seriesId;
    }

    @Override
    public String getSeriesName() {
        return seriesName;
    }

    @Override
    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    @Override
    public Long getBrandId() {
        return brandId;
    }

    @Override
    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
