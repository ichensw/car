package cn.ichensw.system.domain.vo;

import cn.ichensw.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

public class SysGradeVo {
    private static final long serialVersionUID = 1L;

    /**
     * 会员等级ID
     */
    private Long gradeId;

    /**
     * 等级名称
     */
    private String gradeName;

    /**
     * 各等级统计人数
     */
    private Long memberNumber;


    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }

    public Long getGradeId() {
        return gradeId;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public Long getMemberNumber() {
        return memberNumber;
    }

    public void setMemberNumber(Long memberNumber) {
        this.memberNumber = memberNumber;
    }

    public String getGradeName() {
        return gradeName;
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("gradeId" , getGradeId())
                .append("gradeName" , getGradeName())
                .append("memberNumber" , getMemberNumber())
                .toString();
    }
}
