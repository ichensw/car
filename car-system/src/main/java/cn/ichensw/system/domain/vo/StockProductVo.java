package cn.ichensw.system.domain.vo;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.system.domain.StockProduct;

import java.math.BigDecimal;

public class StockProductVo extends StockProduct {

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 产品名称
     */
    @Excel(name = "产品名称")
    private String productName;

    /**
     * 产品规格
     */
    @Excel(name = "产品规格")
    private String productSpecifications;

    /**
     * 产品产地
     */
    @Excel(name = "产品产地")
    private String productAddress;

    /**
     * 产品售价
     */
    @Excel(name = "产品售价")
    private BigDecimal productOutAmount;

    /**
     * 产品进价
     */
    @Excel(name = "产品进价")
    private BigDecimal productInAmount;

    /**
     * 产品图片
     */
    @Excel(name = "产品图片")
    private String productImg;

    /**
     * 产品数量
     */
    @Excel(name = "产品数量")
    private Long productCount;

    /**
     * 产品类别ID
     */
    @Excel(name = "产品类别ID")
    private Long typeId;

    /**
     * 类别名称
     */
    @Excel(name = "类别名称")
    private String typeName;

    @Override
    public Long getProductId() {
        return productId;
    }

    @Override
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @Override
    public String getProductName() {
        return productName;
    }

    @Override
    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public String getProductSpecifications() {
        return productSpecifications;
    }

    @Override
    public void setProductSpecifications(String productSpecifications) {
        this.productSpecifications = productSpecifications;
    }

    @Override
    public String getProductAddress() {
        return productAddress;
    }

    @Override
    public void setProductAddress(String productAddress) {
        this.productAddress = productAddress;
    }

    @Override
    public BigDecimal getProductOutAmount() {
        return productOutAmount;
    }

    @Override
    public void setProductOutAmount(BigDecimal productOutAmount) {
        this.productOutAmount = productOutAmount;
    }

    @Override
    public BigDecimal getProductInAmount() {
        return productInAmount;
    }

    @Override
    public void setProductInAmount(BigDecimal productInAmount) {
        this.productInAmount = productInAmount;
    }

    @Override
    public String getProductImg() {
        return productImg;
    }

    @Override
    public void setProductImg(String productImg) {
        this.productImg = productImg;
    }

    @Override
    public Long getProductCount() {
        return productCount;
    }

    @Override
    public void setProductCount(Long productCount) {
        this.productCount = productCount;
    }

    @Override
    public Long getTypeId() {
        return typeId;
    }

    @Override
    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
