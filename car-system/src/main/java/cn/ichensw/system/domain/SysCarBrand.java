package cn.ichensw.system.domain;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 汽车品牌对象 sys_car_brand
 *
 * @author jucce
 * @date 2022-11-26
 */
public class SysCarBrand extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 汽车品牌ID
     */
    private Long brandId;

    /**
     * 汽车品牌名称
     */
    @Excel(name = "汽车品牌名称")
    private String brandName;

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandName() {
        return brandName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("brandId" , getBrandId())
                .append("brandName" , getBrandName())
                .toString();
    }
}
