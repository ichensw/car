package cn.ichensw.system.domain;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 会员充值对象 member_recharge
 *
 * @author jucce
 * @date 2022-11-25
 */
public class MemberRecharge extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 会员充值ID
     */
    private Long rechargeId;

    /**
     * 充值金额
     */
    @Excel(name = "充值金额")
    private BigDecimal rechargeAmount;

    /**
     * 充值优惠金额
     */
    @Excel(name = "赠送金额")
    private BigDecimal rechargeGiveAmount;


    /**
     * 充值备注
     */
    @Excel(name = "充值备注")
    private String rechargeRemark;

    /**
     * 会员信息ID
     */
    @Excel(name = "会员信息ID")
    private Long memberInfoId;

    /**
     * 管理员ID
     */
    @Excel(name = "管理员ID")
    private Long userId;

    /**
     * 优惠活动ID
     */
    @Excel(name = "优惠活动ID")
    private Long activiesId;

    public void setRechargeId(Long rechargeId) {
        this.rechargeId = rechargeId;
    }

    public Long getRechargeId() {
        return rechargeId;
    }

    public void setRechargeAmount(BigDecimal rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public BigDecimal getRechargeAmount() {
        return rechargeAmount;
    }

    public BigDecimal getRechargeGiveAmount() {
        return rechargeGiveAmount;
    }

    public void setRechargeGiveAmount(BigDecimal rechargeGiveAmount) {
        this.rechargeGiveAmount = rechargeGiveAmount;
    }

    public void setRechargeRemark(String rechargeRemark) {
        this.rechargeRemark = rechargeRemark;
    }

    public String getRechargeRemark() {
        return rechargeRemark;
    }

    public void setMemberInfoId(Long memberInfoId) {
        this.memberInfoId = memberInfoId;
    }

    public Long getMemberInfoId() {
        return memberInfoId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setActiviesId(Long activiesId) {
        this.activiesId = activiesId;
    }

    public Long getActiviesId() {
        return activiesId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("rechargeId" , getRechargeId())
                .append("rechargeAmount" , getRechargeAmount())
                .append("rechargeGiveAmount" , getRechargeGiveAmount())
                .append("rechargeRemark" , getRechargeRemark())
                .append("memberInfoId" , getMemberInfoId())
                .append("userId" , getUserId())
                .append("activiesId" , getActiviesId())
                .toString();
    }
}
