package cn.ichensw.system.domain;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 库存管理对象 stock_manage
 *
 * @author jucce
 * @date 2022-11-25
 */
public class StockManage extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 库存管理ID
     */
    private Long manageId;

    /**
     * 入库数量
     */
    @Excel(name = "入库数量")
    private Long manageCount;

    /**
     * 入库时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入库时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date manageTime;

    /**
     * 产品ID
     */
    @Excel(name = "产品ID")
    private Long productId;

    /**
     * 管理员ID
     */
    @Excel(name = "管理员ID")
    private Long userId;
    /**
     * 产品类别ID
     */
    @Excel(name = "产品类别ID")
    private Long typeId;

    public void setManageId(Long manageId) {
        this.manageId = manageId;
    }

    public Long getManageId() {
        return manageId;
    }

    public void setManageCount(Long manageCount) {
        this.manageCount = manageCount;
    }

    public Long getManageCount() {
        return manageCount;
    }

    public void setManageTime(Date manageTime) {
        this.manageTime = manageTime;
    }

    public Date getManageTime() {
        return manageTime;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("manageId" , getManageId())
                .append("manageCount" , getManageCount())
                .append("manageTime" , getManageTime())
                .append("productId" , getProductId())
                .append("userId" , getUserId())
                .append("typeId" , getTypeId())
                .toString();
    }
}
