package cn.ichensw.system.domain.vo;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 车辆销售对象 order_car
 *
 * @author jucce
 * @date 2023-03-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderCarVO extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * 车辆销售ID
     */
    @Excel(name = "车辆销售ID")
    private Long orderCarId;

    /**
     * 车辆ID
     */
    @Excel(name = "车辆ID")
    private Long carId;

    /**
     * 品牌ID
     */
    @Excel(name = "品牌ID")
    private Long brandId;

    /**
     * 系列ID
     */
    @Excel(name = "系列ID")
    private Long seriesId;

    /**
     * 经办人
     */
    @Excel(name = "经办人")
    private Long userId;

    /**
     * 是否支付（0-未支付，1-已支付）
     */
    @Excel(name = "是否支付", readConverterExp = "0=-未支付，1-已支付")
    private Long isPay;

    /**
     * 支付方式（0-全额付款，1-分期付款）
     */
    @Excel(name = "支付方式", readConverterExp = "0=-全额付款，1-分期付款")
    private Long payMethod;

    /**
     * 客户名称
     */
    @Excel(name = "客户名称")
    private String customerName;
    /**
     * 汽车名称
     */
    @Excel(name = "汽车名称")
    private String carName;

    /**
     * 客户电话
     */
    @Excel(name = "客户电话")
    private String customerPhone;

    /**
     * 经办人账号
     */
    @Excel(name = "经办人账号")
    private String userName;

    /**
     * 经办人名称
     */
    @Excel(name = "经办人名称")
    private String nickName;

    /**
     * 数量
     */
    @Excel(name = "数量")
    private Integer carNum;

    /**
     * 价格
     */
    @Excel(name = "价格")
    private Integer carPrice;

    /**
     * 交付时间
     */
    @Excel(name = "交付时间")
    private Date deliveryTime;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    private Date createTime;


}
