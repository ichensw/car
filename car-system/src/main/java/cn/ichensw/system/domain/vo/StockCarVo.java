package cn.ichensw.system.domain.vo;

import cn.ichensw.common.annotation.Excel;

import java.math.BigDecimal;
import java.util.Date;

public class StockCarVo {

    /** 车辆ID */
    private Long carId;

    /** 车辆系列ID */
    private Long seriesId;

    /** 车辆品牌ID */
    private Long brandId;

    /** 车辆品牌 */
    private String brandName;

    /** 车辆系列 */
    private String seriesName;

    /** 汽车名称 */
    @Excel(name = "汽车名称")
    private String carName;

    /** 座位数 */
    private Long carSeatsNum;

    /** 换挡方式 */
    private String carShift;

    /** 数量 */
    private Long carNum;

    /** 售价 */
    private BigDecimal carPrice;

    /** 车辆图片 */
    private String carImg;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Date createTime;

    @Override
    public String toString() {
        return "StockCarVo{" +
                "carId=" + carId +
                ", seriesId=" + seriesId +
                ", brandId=" + brandId +
                ", brandName='" + brandName + '\'' +
                ", seriesName='" + seriesName + '\'' +
                ", carName='" + carName + '\'' +
                ", carSeatsNum=" + carSeatsNum +
                ", carShift='" + carShift + '\'' +
                ", carNum=" + carNum +
                ", carPrice=" + carPrice +
                ", carImg='" + carImg + '\'' +
                ", createTime=" + createTime +
                '}';
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getSeriesId() {
        return seriesId;
    }

    public void setSeriesId(Long seriesId) {
        this.seriesId = seriesId;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public Long getCarSeatsNum() {
        return carSeatsNum;
    }

    public void setCarSeatsNum(Long carSeatsNum) {
        this.carSeatsNum = carSeatsNum;
    }

    public String getCarShift() {
        return carShift;
    }

    public void setCarShift(String carShift) {
        this.carShift = carShift;
    }

    public Long getCarNum() {
        return carNum;
    }

    public void setCarNum(Long carNum) {
        this.carNum = carNum;
    }

    public BigDecimal getCarPrice() {
        return carPrice;
    }

    public void setCarPrice(BigDecimal carPrice) {
        this.carPrice = carPrice;
    }

    public String getCarImg() {
        return carImg;
    }

    public void setCarImg(String carImg) {
        this.carImg = carImg;
    }
}
