package cn.ichensw.system.domain;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 产品信息对象 stock_product
 *
 * @author jucce
 * @date 2022-11-25
 */
public class StockProduct extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 产品名称
     */
    @Excel(name = "产品名称")
    private String productName;

    /**
     * 产品规格
     */
    @Excel(name = "产品规格")
    private String productSpecifications;

    /**
     * 产品产地
     */
    @Excel(name = "产品产地")
    private String productAddress;

    /**
     * 产品售价
     */
    @Excel(name = "产品售价")
    private BigDecimal productOutAmount;

    /**
     * 产品进价
     */
    @Excel(name = "产品进价")
    private BigDecimal productInAmount;

    /**
     * 产品图片
     */
    @Excel(name = "产品图片")
    private String productImg;

    /**
     * 产品数量
     */
    @Excel(name = "产品数量")
    private Long productCount;

    /**
     * 产品类别ID
     */
    @Excel(name = "产品类别ID")
    private Long typeId;

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductSpecifications(String productSpecifications) {
        this.productSpecifications = productSpecifications;
    }

    public String getProductSpecifications() {
        return productSpecifications;
    }

    public void setProductAddress(String productAddress) {
        this.productAddress = productAddress;
    }

    public String getProductAddress() {
        return productAddress;
    }

    public void setProductOutAmount(BigDecimal productOutAmount) {
        this.productOutAmount = productOutAmount;
    }

    public BigDecimal getProductOutAmount() {
        return productOutAmount;
    }

    public void setProductInAmount(BigDecimal productInAmount) {
        this.productInAmount = productInAmount;
    }

    public BigDecimal getProductInAmount() {
        return productInAmount;
    }

    public void setProductImg(String productImg) {
        this.productImg = productImg;
    }

    public String getProductImg() {
        return productImg;
    }

    public void setProductCount(Long productCount) {
        this.productCount = productCount;
    }

    public Long getProductCount() {
        return productCount;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Long getTypeId() {
        return typeId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("productId" , getProductId())
                .append("productName" , getProductName())
                .append("productSpecifications" , getProductSpecifications())
                .append("productAddress" , getProductAddress())
                .append("productOutAmount" , getProductOutAmount())
                .append("productInAmount" , getProductInAmount())
                .append("productImg" , getProductImg())
                .append("productCount" , getProductCount())
                .append("typeId" , getTypeId())
                .toString();
    }
}
