package cn.ichensw.system.domain;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 汽车系列对象 sys_car_series
 *
 * @author jucce
 * @date 2022-11-26
 */
public class SysCarSeries extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 汽车系列ID
     */
    private Long seriesId;

    /**
     * 汽车系列名称
     */
    @Excel(name = "汽车系列名称")
    private String seriesName;

    /**
     * 汽车品牌ID
     */
    private Long brandId;

    public void setSeriesId(Long seriesId) {
        this.seriesId = seriesId;
    }

    public Long getSeriesId() {
        return seriesId;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getBrandId() {
        return brandId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("seriesId" , getSeriesId())
                .append("seriesName" , getSeriesName())
                .append("brandId" , getBrandId())
                .toString();
    }
}
