package cn.ichensw.system.domain.vo;

import cn.ichensw.common.annotation.Excel;
import cn.ichensw.system.domain.MemberInfo;

import java.math.BigDecimal;

public class MemberInfoVo extends MemberInfo {

    /**
     * 会员等级名称
     */
    private String gradeName;
    /**
     * 汽车系列名称
     */
    private String seriesName;
    /**
     * 证件类型名称
     */
    private String certName;
    /**
     * 汽车品牌名称
     */
    private String brandName;

    /**
     * 等级消费折扣
     */
    @Excel(name = "等级消费折扣")
    private BigDecimal gradeDiscount;


    public BigDecimal getGradeDiscount() {
        return gradeDiscount;
    }

    public void setGradeDiscount(BigDecimal gradeDiscount) {
        this.gradeDiscount = gradeDiscount;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCertName() {
        return certName;
    }

    public void setCertName(String certName) {
        this.certName = certName;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }
}
