package cn.ichensw.system.mapper;

import cn.ichensw.system.domain.MemberInfo;
import cn.ichensw.system.domain.dto.MemberInfoDTO;
import cn.ichensw.system.domain.vo.MemberInfoVo;
import cn.ichensw.system.domain.vo.OrderMemberVo;
import cn.ichensw.system.domain.vo.OrderStatisticsVo;
import cn.ichensw.system.domain.vo.UserStatisticsVo;

import java.util.List;

/**
 * 会员信息Mapper接口
 *
 * @author jucce
 * @date 2022-11-25
 */
public interface MemberInfoMapper {
    /**
     * 查询会员信息
     *
     * @param memberInfoId 会员信息主键
     * @return 会员信息
     */
    public MemberInfo selectMemberInfoByMemberInfoId(Long memberInfoId);

    /**
     * 查询会员信息列表
     *
     * @param memberInfo 会员信息
     * @return 会员信息集合
     */
    public List<MemberInfo> selectMemberInfoList(MemberInfo memberInfo);

    /**
     * 新增会员信息
     *
     * @param memberInfo 会员信息
     * @return 结果
     */
    public int insertMemberInfo(MemberInfo memberInfo);

    /**
     * 修改会员信息
     *
     * @param memberInfo 会员信息
     * @return 结果
     */
    public int updateMemberInfo(MemberInfo memberInfo);

    /**
     * 删除会员信息
     *
     * @param memberInfoId 会员信息主键
     * @return 结果
     */
    public int deleteMemberInfoByMemberInfoId(Long memberInfoId);

    /**
     * 批量删除会员信息
     *
     * @param memberInfoIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMemberInfoByMemberInfoIds(Long[] memberInfoIds);


    /**
     * 通过会员卡号查询会员信息
     *
     * @param memberInfoNumber 会员卡号
     * @return 结果
     */
    String selectMemberIdByMemberInfoNumber(String memberInfoNumber);

    /**
     * 充值会员余额
     *
     * @param memberInfo 会员信息
     * @return 结果
     */
    void addMemberInfoMoney(MemberInfo memberInfo);

    /**
     * 根据卡号查询会员详细信息
     *
     * @param memberInfoNumber 会员卡号
     * @return 会员详细信息
     */
    MemberInfoVo selectMemberInfoByMemberInfoNumber(String memberInfoNumber);

    /**
     * 订单结算--修改会员余额
     *
     * @param orderMemberVo 会员信息
     * @return 修改结果
     */
    int updateMemberInfoMoney(OrderMemberVo orderMemberVo);

    /**
     * 订单结算--修改会员等级
     *
     * @param memberInfoDTO 会员信息
     * @return 修改结果
     */
    int updateMemberInfoGrade(MemberInfoDTO memberInfoDTO);

    /**
     * 根据会员信息ID查询会员部分数据
     *
     * @param memberInfoNumber 会员卡号
     * @return 结果
     */
    MemberInfoDTO selectMemberInfoDTOByMemberInfoId(String memberInfoNumber);


    /**
     * 查询订单总数
     *
     * @return 订单总数
     */
    Long selectOrderCount();

    /**
     * 获取用户总数
     *
     * @return 用户总数
     */
    Long selectUserCount();

    /**
     * 查询每个月的会员注册人数
     *
     * @return 结果
     */
    List<UserStatisticsVo> selectMonthUserCount();


}
