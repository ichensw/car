package cn.ichensw.system.mapper;

import cn.ichensw.system.domain.SysCarSeries;
import cn.ichensw.system.domain.vo.SysCarSeriesVo;

import java.util.List;

/**
 * 汽车系列Mapper接口
 *
 * @author jucce
 * @date 2022-11-26
 */
public interface SysCarSeriesMapper {
    /**
     * 查询汽车系列
     *
     * @param seriesId 汽车系列主键
     * @return 汽车系列
     */
    public SysCarSeries selectSysCarSeriesBySeriesId(Long seriesId);

    /**
     * 查询汽车系列列表
     *
     * @param sysCarSeriesVo 汽车系列
     * @return 汽车系列集合
     */
    public List<SysCarSeriesVo> selectSysCarSeriesList(SysCarSeries sysCarSeries);

    /**
     * 新增汽车系列
     *
     * @param sysCarSeries 汽车系列
     * @return 结果
     */
    public int insertSysCarSeries(SysCarSeries sysCarSeries);

    /**
     * 修改汽车系列
     *
     * @param sysCarSeries 汽车系列
     * @return 结果
     */
    public int updateSysCarSeries(SysCarSeries sysCarSeries);

    /**
     * 删除汽车系列
     *
     * @param seriesId 汽车系列主键
     * @return 结果
     */
    public int deleteSysCarSeriesBySeriesId(Long seriesId);

    /**
     * 批量删除汽车系列
     *
     * @param seriesIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysCarSeriesBySeriesIds(Long[] seriesIds);
}
