package cn.ichensw.system.mapper;

import cn.ichensw.system.domain.OrderMember;
import cn.ichensw.system.domain.vo.OrderMemberVo;
import cn.ichensw.system.domain.vo.OrderStatisticsVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 会员消费Mapper接口
 *
 * @author jucce
 * @date 2022-11-25
 */
public interface OrderMemberMapper {
    /**
     * 查询会员消费
     *
     * @param orderMemberId 会员消费主键
     * @return 会员消费
     */
    public OrderMember selectOrderMemberByOrderMemberId(Long orderMemberId);

    /**
     * 查询会员消费列表
     *
     * @param orderMemberVo 会员消费
     * @return 会员消费集合
     */
    public List<OrderMember> selectOrderMemberList(OrderMemberVo orderMemberVo);

    /**
     * 新增会员消费
     *
     * @param orderMember 会员消费
     * @return 结果
     */
    public int insertOrderMember(OrderMember orderMember);

    /**
     * 修改会员消费
     *
     * @param orderMember 会员消费
     * @return 结果
     */
    public int updateOrderMember(OrderMember orderMember);

    /**
     * 删除会员消费
     *
     * @param orderMemberId 会员消费主键
     * @return 结果
     */
    public int deleteOrderMemberByOrderMemberId(Long orderMemberId);

    /**
     * 批量删除会员消费
     *
     * @param orderMemberIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOrderMemberByOrderMemberIds(Long[] orderMemberIds);

    /**
     * 查询未结算的会员消费订单列表
     *
     * @return 结果
     */
    List<OrderMemberVo> selectOrderMemberListAndNoSettlement();

    /**
     * 订单结算--修改单个订单状态为1
     *
     * @param orderMemberId 订单ID
     * @return 修改结果
     */
    int updateOrderMemberByOne(@Param("orderMemberId") Long orderMemberId);

    /**
     * 查询每个月的订单总数
     *
     * @return 结果
     */
    List<OrderStatisticsVo> selectMonthOrderCount();
}
