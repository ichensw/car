package cn.ichensw.system.mapper;

import java.util.List;

import cn.ichensw.system.domain.StockCar;
import cn.ichensw.system.domain.dto.StockCarDTO;
import cn.ichensw.system.domain.vo.StockCarVo;

/**
 * 车辆信息Mapper接口
 *
 * @author jucce
 * @date 2023-03-05
 */
public interface StockCarMapper {
    /**
     * 查询车辆信息
     *
     * @param carId 车辆信息主键
     * @return 车辆信息
     */
    public StockCar selectStockCarByCarId(Long carId);

    /**
     * 查询车辆信息列表
     *
     * @param stockCar 车辆信息
     * @return 车辆信息集合
     */
    public List<StockCarVo> selectStockCarList(StockCar stockCar);

    /**
     * 新增车辆信息
     *
     * @param stockCar 车辆信息
     * @return 结果
     */
    public int insertStockCar(StockCar stockCar);

    /**
     * 修改车辆信息
     *
     * @param stockCar 车辆信息
     * @return 结果
     */
    public int updateStockCar(StockCar stockCar);

    /**
     * 删除车辆信息
     *
     * @param carId 车辆信息主键
     * @return 结果
     */
    public int deleteStockCarByCarId(Long carId);

    /**
     * 批量删除车辆信息
     *
     * @param carIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockCarByCarIds(Long[] carIds);

    /**
     * 查询汽车列表
     *
     * @param stockCar
     * @return
     */
    List<StockCarDTO> selectStockCarDTOList(StockCar stockCar);
}
