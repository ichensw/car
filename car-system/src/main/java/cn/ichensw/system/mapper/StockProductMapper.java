package cn.ichensw.system.mapper;

import cn.ichensw.system.domain.StockProduct;
import cn.ichensw.system.domain.vo.StockProductVo;

import java.util.List;

/**
 * 产品信息Mapper接口
 *
 * @author jucce
 * @date 2022-11-25
 */
public interface StockProductMapper {
    /**
     * 查询产品信息
     *
     * @param productId 产品信息主键
     * @return 产品信息
     */
    public StockProduct selectStockProductByProductId(Long productId);

    /**
     * 查询产品信息列表
     *
     * @param stockProduct 产品信息
     * @return 产品信息集合
     */
    public List<StockProductVo> selectStockProductList(StockProduct stockProduct);

    /**
     * 新增产品信息
     *
     * @param stockProduct 产品信息
     * @return 结果
     */
    public int insertStockProduct(StockProduct stockProduct);

    /**
     * 修改产品信息
     *
     * @param stockProduct 产品信息
     * @return 结果
     */
    public int updateStockProduct(StockProduct stockProduct);

    /**
     * 删除产品信息
     *
     * @param productId 产品信息主键
     * @return 结果
     */
    public int deleteStockProductByProductId(Long productId);

    /**
     * 批量删除产品信息
     *
     * @param productIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockProductByProductIds(Long[] productIds);

    /**
     * 根据产品类别查询产品信息
     *
     * @param typeId 产品类别ID
     * @return 产品信息
     */
    List<StockProduct> selectStockProductByTypeId(Long typeId);

    /**
     * 订单结算--减少产品库存
     *
     * @param stockProduct 库存信息
     * @return 修改结果
     */
    int reduceStockProductCount(StockProduct stockProduct);

    /**
     * 订单结算--增加产品库存
     *
     * @param stockProduct 库存信息
     * @return 修改结果
     */
    int addStockProductCount(StockProduct stockProduct);

}
