package cn.ichensw.system.mapper;

import cn.ichensw.system.domain.OrderSpread;
import cn.ichensw.system.domain.vo.OrderSpreadVo;

import java.util.List;

/**
 * 散客消费Mapper接口
 *
 * @author jucce
 * @date 2022-11-25
 */
public interface OrderSpreadMapper {
    /**
     * 查询散客消费
     *
     * @param spreadId 散客消费主键
     * @return 散客消费
     */
    public OrderSpread selectOrderSpreadBySpreadId(Long spreadId);

    /**
     * 查询散客消费列表
     *
     * @param orderSpreadVo 散客消费
     * @return 散客消费集合
     */
    public List<OrderSpreadVo> selectOrderSpreadList(OrderSpreadVo orderSpreadVo);

    /**
     * 新增散客消费
     *
     * @param orderSpread 散客消费
     * @return 结果
     */
    public int insertOrderSpread(OrderSpread orderSpread);

    /**
     * 修改散客消费
     *
     * @param orderSpread 散客消费
     * @return 结果
     */
    public int updateOrderSpread(OrderSpread orderSpread);

    /**
     * 删除散客消费
     *
     * @param spreadId 散客消费主键
     * @return 结果
     */
    public int deleteOrderSpreadBySpreadId(Long spreadId);

    /**
     * 批量删除散客消费
     *
     * @param spreadIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOrderSpreadBySpreadIds(Long[] spreadIds);
}
