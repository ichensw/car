package cn.ichensw.system.mapper;

import cn.ichensw.system.domain.SysCustomerCert;

import java.util.List;

/**
 * 客户凭证Mapper接口
 *
 * @author jucce
 * @date 2022-11-26
 */
public interface SysCustomerCertMapper {
    /**
     * 查询客户凭证
     *
     * @param certId 客户凭证主键
     * @return 客户凭证
     */
    public SysCustomerCert selectSysCustomerCertByCertId(Long certId);

    /**
     * 查询客户凭证列表
     *
     * @param sysCustomerCert 客户凭证
     * @return 客户凭证集合
     */
    public List<SysCustomerCert> selectSysCustomerCertList(SysCustomerCert sysCustomerCert);

    /**
     * 新增客户凭证
     *
     * @param sysCustomerCert 客户凭证
     * @return 结果
     */
    public int insertSysCustomerCert(SysCustomerCert sysCustomerCert);

    /**
     * 修改客户凭证
     *
     * @param sysCustomerCert 客户凭证
     * @return 结果
     */
    public int updateSysCustomerCert(SysCustomerCert sysCustomerCert);

    /**
     * 删除客户凭证
     *
     * @param certId 客户凭证主键
     * @return 结果
     */
    public int deleteSysCustomerCertByCertId(Long certId);

    /**
     * 批量删除客户凭证
     *
     * @param certIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysCustomerCertByCertIds(Long[] certIds);
}
