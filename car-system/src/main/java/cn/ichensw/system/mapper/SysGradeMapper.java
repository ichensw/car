package cn.ichensw.system.mapper;

import cn.ichensw.system.domain.SysGrade;
import cn.ichensw.system.domain.vo.SysGradeVo;

import java.util.List;

/**
 * 会员等级Mapper接口
 *
 * @author jucce
 * @date 2022-11-25
 */
public interface SysGradeMapper {
    /**
     * 查询会员等级
     *
     * @param gradeId 会员等级主键
     * @return 会员等级
     */
    public SysGrade selectSysGradeByGradeId(Long gradeId);

    /**
     * 查询会员等级列表
     *
     * @param sysGrade 会员等级
     * @return 会员等级集合
     */
    public List<SysGrade> selectSysGradeList(SysGrade sysGrade);

    /**
     * 新增会员等级
     *
     * @param sysGrade 会员等级
     * @return 结果
     */
    public int insertSysGrade(SysGrade sysGrade);

    /**
     * 修改会员等级
     *
     * @param sysGrade 会员等级
     * @return 结果
     */
    public int updateSysGrade(SysGrade sysGrade);

    /**
     * 删除会员等级
     *
     * @param gradeId 会员等级主键
     * @return 结果
     */
    public int deleteSysGradeByGradeId(Long gradeId);

    /**
     * 批量删除会员等级
     *
     * @param gradeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysGradeByGradeIds(Long[] gradeIds);

    /**
     * 查询会员等级 和 各等级对应人数
     *
     * @return 结果
     */
    List<SysGradeVo> selectSysGradeAndMemberNumber();

    /**
     * 根据会员积分判断等级
     *
     * @param memberInfoIntegral 会员积分
     * @return 会员等级
     */
    Long selectSysGradeByGradeIntegral(Long memberInfoIntegral);
}
