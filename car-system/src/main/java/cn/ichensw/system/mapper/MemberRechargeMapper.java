package cn.ichensw.system.mapper;

import cn.ichensw.system.domain.MemberInfo;
import cn.ichensw.system.domain.MemberRecharge;
import cn.ichensw.system.domain.vo.MemberRechargeVo;

import java.util.List;

/**
 * 会员充值Mapper接口
 *
 * @author jucce
 * @date 2022-11-25
 */
public interface MemberRechargeMapper {
    /**
     * 查询会员充值
     *
     * @param rechargeId 会员充值主键
     * @return 会员充值
     */
    public MemberRecharge selectMemberRechargeByRechargeId(Long rechargeId);

    /**
     * 查询会员充值列表
     *
     * @param memberRechargeVo 会员充值
     * @return 会员充值集合
     */
    public List<MemberRechargeVo> selectMemberRechargeList(MemberRechargeVo memberRechargeVo);

    /**
     * 新增会员充值
     *
     * @param memberRecharge 会员充值
     * @return 结果
     */
    public int insertMemberRecharge(MemberRecharge memberRecharge);

    /**
     * 修改会员充值
     *
     * @param memberRecharge 会员充值
     * @return 结果
     */
    public int updateMemberRecharge(MemberRecharge memberRecharge);

    /**
     * 删除会员充值
     *
     * @param rechargeId 会员充值主键
     * @return 结果
     */
    public int deleteMemberRechargeByRechargeId(Long rechargeId);

    /**
     * 批量删除会员充值
     *
     * @param rechargeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMemberRechargeByRechargeIds(Long[] rechargeIds);

    /**
     * 校验会员支付密码是否正确
     *
     * @param memberInfo 会员信息
     * @return 结果
     */
    Integer checkNumberAndPassword(MemberInfo memberInfo);

}
