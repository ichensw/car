<p align="center">
	<img alt="logo" src="https://image-bed-ichensw.oss-cn-hangzhou.aliyuncs.com/typora-images/202212060939645.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">Car v1.0.0</h1>
<h4 align="center">基于ssm的汽车及配件营销服务管理系统设计与实现</h4>


## 平台简介

* 前端采用Vue、Element UI。
* 后端采用Spring Boot、Spring Security、Redis & Jwt。
* 权限认证使用Jwt，支持多终端认证系统。
* 支持加载动态权限菜单，多方式轻松权限控制。

## 功能模块

1.  系统设置：工作人员是系统操作者，该功能主要完成系统必要配置和系统相关数据录入。
2.  会员信息管理：会员信息的录入和管理。
3.  会员充值：会员充值功能。
4.  产品信息管理：汽车店产品的录入和管理。
5.  库存管理：产品的入库和出库功能。
6.  会员消费管理：会员消费功能，并且实现了会员购物车功能。
7.  散客消费管理：未注册会员用户使用，直接消费并扣除库存。
8.  客户登记统计：Echart图展示会员人数和等级分布概况。

## 在线体验

演示地址：https://car.ichensw.cn

- 账号：admin
- 密码：123456

## 演示图

<table>
    <tr>
        <td><img src="https://image-bed-ichensw.oss-cn-hangzhou.aliyuncs.com/typora-images/202212060958732.png"/></td>
        <td><img src="https://image-bed-ichensw.oss-cn-hangzhou.aliyuncs.com/typora-images/202212061000140.png"/></td>
    </tr>
    <tr>
        <td><img src="https://image-bed-ichensw.oss-cn-hangzhou.aliyuncs.com/typora-images/202212061000069.png"/></td>
        <td><img src="https://image-bed-ichensw.oss-cn-hangzhou.aliyuncs.com/typora-images/202212061003347.png"/></td>
    </tr>
    <tr>
        <td><img src="https://image-bed-ichensw.oss-cn-hangzhou.aliyuncs.com/typora-images/202212061004724.png"/></td>
        <td><img src="https://image-bed-ichensw.oss-cn-hangzhou.aliyuncs.com/typora-images/202212061004566.png"/></td>
    </tr>
	<tr>
        <td><img src="https://image-bed-ichensw.oss-cn-hangzhou.aliyuncs.com/typora-images/202212061004616.png"/></td>
        <td><img src="https://image-bed-ichensw.oss-cn-hangzhou.aliyuncs.com/typora-images/202212061005697.png"/></td>
    </tr>	 
    <tr>
        <td><img src="https://image-bed-ichensw.oss-cn-hangzhou.aliyuncs.com/typora-images/202212061005492.png"/></td>
        <td><img src="https://image-bed-ichensw.oss-cn-hangzhou.aliyuncs.com/typora-images/202212061006238.png"/></td>
    </tr>
</table>



