create table `ssm-car`.order_spread
(
    spread_id    bigint auto_increment comment '散客消费ID'
        primary key,
    spread_count int         null comment '购买数量',
    spread_name  varchar(20) null comment '购买姓名',
    spread_phone varchar(20) null comment '购买电话',
    spread_time  datetime    null comment '购买时间',
    product_id   bigint      null comment '产品ID',
    user_id      bigint      null comment '管理员ID',
    constraint order_spread_stock_product_null_fk
        foreign key (product_id) references `ssm-car`.stock_product (product_id),
    constraint order_spread_sys_user_null_fk
        foreign key (user_id) references `ssm-car`.sys_user (user_id)
)
    comment '散客消费表';

