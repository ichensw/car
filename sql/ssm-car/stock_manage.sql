create table `ssm-car`.stock_manage
(
    manage_id    bigint auto_increment comment '库存管理ID'
        primary key,
    manage_count int      null comment '入库数量',
    manage_time  datetime null comment '入库时间',
    product_id   bigint   null comment '产品ID',
    user_id      bigint   null comment '管理员ID',
    type_id      bigint   null comment '产品类别ID',
    constraint stock_manage_stock_product_null_fk
        foreign key (product_id) references `ssm-car`.stock_product (product_id),
    constraint stock_manage_sys_product_type_type_id_fk
        foreign key (type_id) references `ssm-car`.sys_product_type (type_id),
    constraint stock_manage_sys_user_null_fk
        foreign key (user_id) references `ssm-car`.sys_user (user_id)
)
    comment '库存管理';

