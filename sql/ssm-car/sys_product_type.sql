create table `ssm-car`.sys_product_type
(
    type_id   bigint auto_increment comment '产品类别ID'
        primary key,
    type_name varchar(50) null comment '产品类别名称'
)
    comment '产品类型表';

