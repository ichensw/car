create table `ssm-car`.stock_product
(
    product_id             bigint auto_increment comment '产品ID'
        primary key,
    product_name           varchar(50)    null comment '产品名称',
    product_specifications varchar(50)    null comment '产品规格',
    product_address        varchar(50)    null comment '产品产地',
    product_out_amount     decimal(10, 2) null comment '产品售价',
    product_in_amount      decimal(10, 2) null comment '产品进价',
    product_img            varchar(255)   null comment '产品图片',
    product_count          int default 0  null comment '产品数量',
    type_id                bigint         null comment '产品类别ID',
    constraint stock_product_sys_product_type_null_fk
        foreign key (type_id) references `ssm-car`.sys_product_type (type_id)
)
    comment '产品信息表';

