create table `ssm-car`.sys_car_series
(
    series_id   bigint auto_increment comment '汽车系列ID'
        primary key,
    series_name varchar(50) not null comment '汽车系列名称',
    brand_id    bigint      null comment '汽车品牌ID',
    constraint sys_car_series_sys_car_brand_null_fk
        foreign key (brand_id) references `ssm-car`.sys_car_brand (brand_id)
)
    comment '汽车系列表';

