create table `ssm-car`.member_info
(
    member_info_id          bigint auto_increment comment '会员信息ID'
        primary key,
    member_info_number      varchar(50)    null comment '会员卡号',
    member_info_name        varchar(50)    null comment '会员名称',
    member_info_money       decimal(10, 2) null comment '会员余额',
    member_info_phone       varchar(30)    null comment '会员电话',
    member_info_password    varchar(50)    null comment '会员支付密码',
    member_info_sex         char           null comment '会员性别(0:男 1:女)',
    member_info_birthday    datetime       null comment '会员生日',
    member_info_integral    int            null comment '会员积分',
    member_info_car_number  varchar(50)    null comment '车牌号',
    member_info_car_color   varchar(20)    null comment '汽车颜色',
    member_info_mileage     decimal(10, 2) null comment '行驶里程',
    member_info_cert_number varchar(50)    null comment '会员证件编号',
    member_info_address     varchar(100)   null comment '会员居住地址',
    member_info_create_time datetime       null comment '会员开通时间',
    member_info_remark      varchar(255)   null comment '会员备注',
    grade_id                bigint         null comment '会员等级ID',
    series_id               bigint         null comment '汽车系列ID',
    cert_id                 bigint         null comment '证件ID',
    brand_id                bigint         null comment '汽车品牌ID',
    constraint member_info_pk
        unique (member_info_number),
    constraint member_info_sys_car_brand_brand_id_fk
        foreign key (brand_id) references `ssm-car`.sys_car_brand (brand_id),
    constraint member_info_sys_car_series_null_fk
        foreign key (series_id) references `ssm-car`.sys_car_series (series_id),
    constraint member_info_sys_customer_cert_null_fk
        foreign key (cert_id) references `ssm-car`.sys_customer_cert (cert_id),
    constraint member_info_sys_grade_null_fk
        foreign key (grade_id) references `ssm-car`.sys_grade (grade_id)
)
    comment '会员信息表';

