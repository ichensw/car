create table `ssm-car`.sys_activies
(
    activies_id              bigint auto_increment comment '优惠活动ID'
        primary key,
    activies_name            varchar(255)   null comment '优惠活动名称',
    activies_begin_time      datetime       null comment '开始时间',
    activies_end_time        datetime       null comment '结束时间',
    activies_amount          decimal(10, 2) null comment '赠送金额',
    activies_recharge_amount decimal(10, 2) null comment '充值金额'
)
    comment '优惠活动表';

