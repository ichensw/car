create table `ssm-car`.order_car
(
    order_car_id   bigint       null comment '车辆销售ID',
    car_id         bigint       null comment '车辆ID',
    user_id        bigint       null comment '经办人',
    is_pay         int          null comment '是否支付（0-未支付，1-已支付）',
    pay_method     int          null comment '支付方式（0-全额付款，1-分期付款）',
    customer_name  varchar(255) null comment '客户名称',
    customer_phone varchar(255) null comment '客户电话'
)
    comment '车辆销售表';

