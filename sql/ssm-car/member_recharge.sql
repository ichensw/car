create table `ssm-car`.member_recharge
(
    recharge_id          bigint auto_increment comment '会员充值ID'
        primary key,
    recharge_amount      decimal(10, 2)   null comment '充值金额',
    recharge_give_amount decimal(10, 2)   null comment '充值赠送金额',
    recharge_remark      varchar(255)     null comment '充值备注',
    member_info_id       bigint           null comment '会员信息ID',
    user_id              bigint           null comment '管理员ID',
    activies_id          bigint default 0 null comment '优惠活动ID(0:代表未选择活动)',
    constraint member_recharge_member_info_null_fk
        foreign key (member_info_id) references `ssm-car`.member_info (member_info_id),
    constraint member_recharge_sys_user_null_fk
        foreign key (user_id) references `ssm-car`.sys_user (user_id)
)
    comment '会员充值表';

create index member_recharge_sys_activies_null_fk
    on `ssm-car`.member_recharge (activies_id);

