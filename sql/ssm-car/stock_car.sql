create table `ssm-car`.stock_car
(
    car_id        bigint auto_increment comment '车辆ID'
        primary key,
    series_id     bigint         null comment '车辆系列',
    car_seats_num int            null comment '座位数',
    car_shift     varchar(255)   null comment '换挡方式',
    car_num       int            null comment '数量',
    car_price     decimal(10, 2) null comment '售价',
    car_img       varchar(255)   null comment '车辆图片'
)
    comment '车辆信息表';

