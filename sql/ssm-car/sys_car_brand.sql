create table `ssm-car`.sys_car_brand
(
    brand_id   bigint auto_increment comment '汽车品牌ID'
        primary key,
    brand_name varchar(50) not null comment '汽车品牌名称'
)
    comment '汽车品牌表';

