create table `ssm-car`.sys_customer_cert
(
    cert_id   bigint auto_increment comment '证件ID'
        primary key,
    cert_name varchar(50) not null comment '证件类型 '
)
    comment '客户凭证表';

