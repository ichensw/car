create table `ssm-car`.order_member
(
    order_member_id     bigint auto_increment comment '会员消费ID'
        primary key,
    order_member_count  int              null comment '购买数量',
    order_member_status char default '0' null comment '会员订单状态(0:未结账 1:已结账)',
    create_time         datetime         null comment '创建时间',
    member_info_id      bigint           null comment '会员信息ID',
    user_id             bigint           null comment '管理员ID',
    product_id          bigint           null comment '产品ID',
    constraint consumption_member_stock_product_null_fk
        foreign key (product_id) references `ssm-car`.stock_product (product_id),
    constraint consumption_member_sys_user_null_fk
        foreign key (user_id) references `ssm-car`.sys_user (user_id)
)
    comment '会员消费表';

