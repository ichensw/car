create table `ssm-car`.sys_grade
(
    grade_id       bigint auto_increment comment '会员等级ID'
        primary key,
    grade_name     varchar(30)    not null comment '等级名称',
    grade_integral int            not null comment '等级所需积分',
    grade_discount decimal(10, 2) not null comment '等级消费折扣'
);

